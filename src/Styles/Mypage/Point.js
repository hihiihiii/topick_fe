import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1400];
const mq = breakpoint.map((bp) => `@media (max-width : ${bp}px)`); // 반응형 작업

const PointTitleFlexBox = styled.div`
  width: 235px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mq[0]} {
    width: 165px;
  }
`;

const PointBox = styled.div`
  width: 110px;
  height: 40px;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  border-radius: 4px;
  background-color: ${(props) => {
    if (props.action) {
      return "#b975fd";
    }
    return "#f5f5f5";
  }};

  color: ${(props) => {
    if (props.action) {
      return "#fff";
    }
    return "#575757";
  }};
  font-size: 13px;
  font-weight: 600;

  ${mq[0]} {
    width: 75px;
    height: 35x;
  }
`;

const PointMain = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;
  justify-content: space-between;
  margin-top: 20px;
  width: 100%;

  ${mq[1]} {
    flex-direction: column;
    justify-content: center;
  }
`;

const PointText = styled.div`
  font-size: ${(props) => {
    if (props.sub) {
      return "14px";
    }
    if (props.point) {
      return "16px";
    }
    return "22px";
  }};
  font-weight: ${(props) => {
    if (props.sub) {
      return "500";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.sub) {
      return "#b7b7b7";
    }
    return "#000";
  }};
`;

const PointHistory = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 374px;
  height: 70px;
  border-radius: 10px;
  background-color: #fff;
  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const PointImg = styled.div`
  width: 38px;
  height: 38px;
  border-radius: 999px;
  background-color: #f0f0f0;
  margin-right: 20px;

  ${mq[0]} {
    display: ${(props) => {
      if (props.history) {
        return "none";
      }
    }};
  }
`;

const PointFlexColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const PointHistoryBox = styled.div`
  display: flex;
  flex-direction: column;
  ${mq[1]} {
    margin-top: 141px;
  }
  ${mq[0]} {
    margin-top: 71px;
    width: 100%;
  }
`;

const FlexBox = styled.div`
  display: flex;
  align-items: center;
`;

export {
  PointHistoryBox,
  FlexBox,
  PointTitleFlexBox,
  PointBox,
  PointMain,
  PointText,
  PointHistory,
  PointImg,
  PointFlexColumn,
};
