import styled from "@emotion/styled";

const breakpoints = [450, 768, 992, 1200, 1425]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const Inner = styled.div`
  position: absolute;
  top: 125.5px;
  left: 435px;
  display: flex;
  flex-direction: column;
  width: 800.5px;

  ${mq[1]} {
    left: 67px;
    width: 634px;
    box-sizing: border-box;
  }
  ${mq[0]} {
    left: 0%;
    padding: 0px 20px;
    box-sizing: border-box;
    width: 100%;
  }
`;

const ProfileHeader = styled.div`
  width: 100%;
  border-bottom: 1px solid #ededed;
  height: 60px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const ProfileTitle = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #000;

  ${mq[0]} {
    font-size: 18px;
  }
`;

const ProfileFlex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: flex-start;

  ${mq[0]} {
    flex-direction: column-reverse;
  }
`;

const ProfileInfomationBox = styled.div`
  display: flex;
  flex-direction: column;
  ${mq[0]} {
    width: 100%;
  }
`;

const ProfileStapline = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: ${(props) => {
    if (props.approval) {
      return "#b975fd";
    }
    return "#000";
  }};
  margin: ${(props) => {
    if (props.approval) {
      return "0px";
    }
    return "20px 0px 10px 0px";
  }};

  ${mq[0]} {
    display: ${(props) => {
      if (props.mobile) {
        return "none";
      }
    }};
    font-size: 13px;
  }
`;

const ProfileInput = styled.input`
  width: ${(props) => {
    if (props.box) {
      return "80%";
    }
    return "425px";
  }};
  height: 40px;
  padding: 0px 13px;
  border-radius: 4px;
  box-sizing: border-box;
  border: solid
    ${(props) => {
      if (props.box) {
        return "0px";
      }
      return "1px";
    }}
    #ebebeb;
  background-color: #fff;
  ${mq[0]} {
    width: ${(props) => {
      if (props.box) {
        return "80%";
      }
      return "100%";
    }};
  }
`;

const ProfileDivBox = styled.div`
  width: ${(props) => {
    if (props.box) {
      return "80%";
    }
    return "425px";
  }};
  height: 40px;
  padding: 0px 18px;
  border-radius: 4px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-left: 12px;
  box-sizing: border-box;
  font-size: 14px;
  color: #a7a7a7;
  border: solid
    ${(props) => {
      if (props.box) {
        return "0px";
      }
      return "1px";
    }}
    #ebebeb;
  background-color: #fff;
  ${mq[0]} {
    width: ${(props) => {
      if (props.box) {
        return "80%";
      }
      return "100%";
    }};
  }
`;

const ProfileInputBox = styled.div`
  width: 100%;
  height: 98.5%;
  box-sizing: border-box;
  border-radius: 4px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding-right: 14px;
  border: 1px solid #ededed;
  background-color: #fff;
  ${mq[0]} {
    width: 100%;
  }
`;

const ProfileImgBox = styled.div`
  display: flex;
  flex-direction: column;
  margin-left: 82px;
  ${mq[1]} {
    margin-left: 60px;
  }
  ${mq[0]} {
    margin-left: 37%;
    justify-content: center;
  }
`;

const ProfileImg = styled.div`
  margin-top: 25px;
  width: 150px;
  height: 150px;
  border-radius: 999px;
  background-color: #f5f5f5;
  position: relative;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);

  ${mq[0]} {
    width: 105px;
    height: 105px;
  }
`;

const ProfileAddBox = styled.div`
  width: 43px;
  height: 43px;
  border-radius: 999px;
  background-color: #bebebe;
  position: absolute;
  bottom: 0;
  right: 0;

  ${mq[0]} {
    width: 30px;
    height: 30px;
  }
`;
export {
  ProfileImgBox,
  ProfileAddBox,
  ProfileImg,
  Inner,
  ProfileHeader,
  ProfileTitle,
  ProfileInfomationBox,
  ProfileFlex,
  ProfileStapline,
  ProfileInput,
  ProfileInputBox,
  ProfileDivBox,
};
