import styled from "@emotion/styled";

const breakpoints = [450, 768, 1400];
const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const TodayWrapper = styled.div`
  margin: 0px auto;
  padding: 0px 155px;

  ${mq[1]} {
    padding: 0px 50px;
  }
  ${mq[0]} {
    padding: 0px 27px;
  }
`;

const TodayTitleBox = styled.div`
  width: 100%;
  height: 85px;
  border-radius: 5px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 35px;

  ${mq[1]} {
    width: 668px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const TodayContent = styled.div`
  width: 333px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${mq[0]} {
    width: 267px;
  }
`;

const TodayTitleText = styled.div`
  width: 98px;
  height: 22px;
  font-size: 18px;
  font-weight: 600;
  color: #000;
  ${mq[0]} {
    width: 82px;
    font-size: 15px;
  }
`;

const TodayVersusText = styled.div`
  width: 15px;
  height: 18px;
  font-size: 14px;
  font-weight: 500;
  color: #9d9d9d;
`;

const TodayTeamIcon = styled.div`
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  width: 21px;
  height: 29px;
`;

const TodayFlexBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const TodayCard = styled.div`
  width: 230px;
  height: 243px;
  border-radius: 5px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  padding: 20px;
  box-sizing: border-box;
  margin-top: 16px;
  cursor: pointer;

  :hover {
    background-color: #ededed;
  }

  ${mq[1]} {
    width: 210px;
  }
  ${mq[0]} {
    height: 157px;
    margin-right: 10px;
  }
`;

const TodayCardBox = styled.div`
  width: 748px;
  display: flex;
  flex-direction: column;
  ${mq[0]} {
    overflow-x: scroll;
  }
`;

const TodayCardFlex = styled.div`
  display: flex;
  width: 100%;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;

  ${mq[0]} {
    overflow-x: scroll;
    width: fit-content;
    flex-wrap: nowrap;
  }
`;

const TodayContentText = styled.div`
  width: 190px;
  height: 66px;
  font-size: 14px;
  margin-top: 10px;
  font-weight: 500;
  line-height: 1.71;
  color: #000;

  ${mq[1]} {
    font-size: 13px;
    width: 170px;
  }
`;

const TodayContentFlex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const TodayCardTitle = styled.div`
  color: #000;
  font-size: 20px;
  font-weight: 500;
  margin-left: 10px;

  ${mq[1]} {
    font-size: 18px;
  }
`;

const TodayProfileImg = styled.div`
  width: 27px;
  height: 27px;
  background-color: #f7f7f7;
  border-radius: 999px;
`;

export {
  TodayContentText,
  TodayContentFlex,
  TodayCardTitle,
  TodayProfileImg,
  TodayCardFlex,
  TodayCardBox,
  TodayCard,
  TodayFlexBox,
  TodayWrapper,
  TodayTitleBox,
  TodayContent,
  TodayTitleText,
  TodayVersusText,
  TodayTeamIcon,
};
