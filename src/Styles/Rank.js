import styled from "@emotion/styled";

const RankFlexBox = styled.div`
  width: 180px;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const RankBtnBox = styled.div`
  width: 85px;
  height: 35px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 16px;
  box-sizing: border-box;
  color: #000;
  border-radius: 100px;
  border: solid 1px #ededed;
  cursor: pointer;
  background-color: ${(props) => {
    if (props.action) {
      return "#b975fd";
    }
    return "#fff";
  }};

  color: ${(props) => {
    if (props.action) {
      return "#fff";
    }
    return "#000";
  }}; ;
`;

export { RankFlexBox, RankBtnBox };
