import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const breakpoints = [470, 768, 992, 1200, 1425]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const Wrapper = styled.div`
  margin: 30px 30px;
`;

const Test = styled.div`
  display: flex;
  justify-content: center;
  height: 100vh;
`;

const Container = styled.div`
  width: 400px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const MainText = styled.div`
  font-size: ${(props) => {
    if (props.title) {
      return "32px";
    }
    if (props.board) {
      return "22px";
    }
    return "14px";
  }};
  font-weight: 600;
  align-self: ${(props) => {
    if (props.center) {
      return "center";
    }
    return "flex-start";
  }};
  margin-bottom: 10px;
`;

const BottomText = styled.div`
  font-size: 15px;
  font-weight: 600;
  color: #b9b9b9;
`;

const MainInputBox = styled.input`
  width: 100%;
  height: 40px;
  box-sizing: border-box;
  padding-left: 18px;
  border-radius: 4px;
  border: solid 1px #ebebeb;
`;

const MainBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => {
    if (props.back) {
      return "#fff";
    }
    return "#b068f8";
  }};
  border-radius: 4px;
  width: 100%;
  height: 50px;
  font-size: 18px;
  cursor: pointer;
  border: solid 1px #ebebeb;
  color: ${(props) => {
    if (props.color) {
      return "#b068f8";
    }
    return "#fff";
  }};

  :hover {
    color: #fff;
    background-color: #b068f8;
  }
`;

const Logo = styled.div`
  width: 300px;
  height: 39px;

  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  background-size: contain;
`;

const MainContainer = styled.div`
  margin: 0px 75px;
  padding-top: 54px;
  position: relative;
  ${mq[4]} {
    padding-top: 20px;
  }
  ${mq[1]} {
    margin: 0px 35px;
  }
  ${mq[0]} {
    margin: 0px 35px;
  }
`;

const MainTitleBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 10px;
`;

const PicksterBoard = styled.div`
  width: 535px;
  margin-right: 25px;
  height: 268px;
  display: flex;
  flex-direction: column;

  ${mq[4]} {
    width: 100%;
  }

  ${mq[1]} {
    width: 100%;
    margin-right: 0px;
    margin-top: 20px;
  }
  ${mq[0]} {
    width: 100%;
    margin-right: 0px;
    height: ${(props) => {
      if (props.mobile) {
        return "180px";
      }
      return "";
    }};
  }
`;

const PicksterText = styled.div`
  font-size: ${(props) => {
    if (props.board) {
      return "22px";
    }
    return "15px";
  }};
  font-weight: 600;
  color: ${(props) => {
    if (props.more) {
      return "#888";
    }
    return "#000";
  }};
  height: 28px;
  display: flex;
  align-items: center;
  cursor: ${(props) => {
    if (props.more) {
      return "pointer";
    }
    return "";
  }};
`;

const MobileOverflow = styled.div``;

const PicksterMain = styled.div`
  box-sizing: border-box;
  width: 100%;
  height: 50px;
  border-radius: 10px;
  cursor: pointer;
  border: 1px solid #f5f5f5;
  padding: 0px 16px;
  display: flex;
  font-weight: 500;
  font-size: 16px;
  align-items: center;
  ${mq[0]} {
    width: 100%;
  }
  ${mq[1]} {
    width: 100%;
  }
`;

const MainTopBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  ${mq[4]} {
    flex-direction: column;
  }
  ${mq[1]} {
    flex-direction: column;
    width: 100%;
  }
  ${mq[0]} {
    flex-direction: column;
    width: 100%;
  }
`;

const PicksterRank = styled.div`
  width: ${(props) => {
    if (props.cate) {
      return "6%";
    }
    return "44%";
  }};
  height: ${(props) => {
    if (props.cate) {
      return "35px";
    }
    return "65px";
  }};
  border: 1px solid #f5f5f5;
  border-radius: 100px;
  padding-left: 22px;
  ${mq[0]} {
    display: none;
  }
  ${mq[1]} {
  }
`;

const MobilePicksterRank = styled.div`
  ${mq[1]} {
    display: none;
  }
  ${mq[0]} {
    display: flex;

    flex-direction: column;
    width: 140px;
    height: 140px;
    border-radius: 16px;
    background-color: #fff;
    border: solid 1px #f5f5f5;
    margin-right: 15px;
  }
  display: none;
`;

const MobilePicsterContent = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-top: 15px;
  width: 100%;
`;

const MobileRankIcon = styled.div`
  width: 42px;
  height: 22px;
  border-radius: 999px;
  background-color: #acc5ff;
`;

const MobileRankText = styled.div`
  display: flex;
  height: 22px;

  align-items: center;
  justify-content: center;
  color: #fff;
  font-size: 12px;
  font-weight: bold;
`;

const CategoryLink = styled.div`
  width: 100px;
  height: 35px;
  border: 1px solid #f5f5f5;
  border-radius: 100px;
  padding-left: 22px;
  text-decoration: none;
  cursor: pointer;
`;

const RankBox = styled.div`
  width: fit-content;
  display: flex;
  flex-wrap: wrap;
  flex-direction: row;
  justify-content: space-between;

  ${mq[0]} {
    overflow-x: scroll;
    flex-wrap: nowrap;
  }

  ${mq[4]} {
  }
`;

const OverflowBox = styled.div`
  ${mq[0]} {
    width: 100%;
    overflow-x: scroll;
    -ms-overflow-style: none;
  }
`;
const ProfileImg = styled.div`
  width: ${(props) => {
    if (props.mobile) {
      return "53px";
    }
    return "36px";
  }};
  height: ${(props) => {
    if (props.mobile) {
      return "53px";
    }
    return "36px";
  }};
  border-radius: 100px;

  background-color: #f8f8f8;
  ${mq[0]} {
    margin-bottom: 15px;
  }
`;

const RankText = styled.div`
  font-size: ${(props) => {
    if (props.num) {
      return "16px";
    }
    if (props.mobile) {
      return "15px";
    }
    return "18px";
  }};

  color: ${(props) => {
    if (props.num) {
      return "#c1c1c1";
    } else {
      return "#000";
    }
  }};

  font-weight: ${(props) => {
    if (props.num) {
      return "bold";
    }
    return 600;
  }};
`;

const RankContent = styled.div`
  display: flex;
  height: ${(props) => {
    if (props.cate) {
      return "35px";
    }
    return "60px";
  }};
  width: ${(props) => {
    if (props.cate) {
      return "70px";
    }
    return "112px";
  }};
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const MainMiddleBox = styled.div`
  margin-top: 45px;
  ${mq[0]} {
    display: none;
  }
  ${mq[1]} {
    display: none;
  }
`;

const MiddleFlex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;

  ${mq[1]} {
  }
  ${mq[0]} {
    width: fit-content;
  }
`;

const Icon = styled.div`
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  background-size: contain;
  width: 19px;
  height: 19px;
`;

const MainBottomBox = styled.div`
  width: 100%;
  margin-top: 37px;
  border-radius: 10px;
  /* overflow-x: scroll; */
  ${mq[1]} {
    overflow-x: scroll;
  }
  ${mq[0]} {
    overflow-x: scroll;
  }
`;

const MainBottomText = styled.div`
  font-size: ${(props) => {
    if (props.bottom) {
      return "14px";
    }
    return "13px";
  }};
  font-weight: ${(props) => {
    if (props.bottom) {
      return "bold";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.bottom) {
      return "#000";
    }
    return "#888";
  }};
`;

const MainBottomTitle = styled.div`
  width: 100%;
  height: 57px;
  background-color: #f8f8f8;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const MainBottomContent = styled.div`
  width: 100%;
  height: 60px;
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const StateBtn = styled.div`
  width: 51px;
  height: 28px;
  border-radius: 4px;

  background-color: ${(props) => {
    if (props.state === "취소") {
      return "#ffdede";
    }
    if (props.state === "완료") {
      return "#ededed";
    }
    if (props.state === "진행중") {
      return "#dee7ff";
    }
  }};
  color: ${(props) => {
    if (props.state === "취소") {
      return "#f85454";
    }
    if (props.state === "완료") {
      return "#000";
    }

    if (props.state === "진행중") {
      return "#4171f2";
    }
  }};

  display: flex;
  align-items: center;
  justify-content: center;
`;

const ChatBox = styled.div`
  position: fixed;
  top: 90%;
  right: 5%;
  width: 150px;
  height: 55px;
  border-radius: 100px;
  background-color: #b068f8;
  display: flex;
  align-items: center;
  justify-content: center;
  z-index: 1;
  cursor: pointer;
`;

const ChatIcon = styled.div`
  width: 20px;
  height: 18px;
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
`;

const FitContent = styled.div`
  ${mq[0]} {
    width: fit-content;
    overflow-x: scroll;
  }
  ${mq[1]} {
    width: fit-content;
    overflow-x: scroll;
  }
`;

const ChatText = styled.div`
  width: 60px;
  height: 18px;
  font-size: 16px;
  font-weight: 500;
  color: #fff;
`;

const ChatContainerBox = styled.div`
  position: relative;
`;

const LogoImg = styled.img`
  width: ${(props) => {
    if (props.main) {
      return "170px";
    }
    if (props.font) {
      return "160px";
    }
    if (props.icon) {
      return "37px";
    }
    return "275px";
  }};
  height: ${(props) => {
    if (props.icon) {
      return "80px";
    }
    return "200px";
  }};
  object-fit: contain;
  margin-bottom: ${(props) => {
    if (props.sub) {
      return "20px";
    }
    if (props.icon) {
      return "5px";
    }
  }};
  margin-top: ${(props) => {
    if (props.font) {
      return "30px";
    }
  }};
  opacity: 0.87;

  ${mq[0]} {
    width: 170px;
  }
`;

const MainLogo = styled.div`
  ${mq[1]} {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 100%;
  }
`;

export {
  MainLogo,
  LogoImg,
  FitContent,
  MobilePicsterContent,
  OverflowBox,
  MobilePicksterRank,
  MobileRankIcon,
  MobileRankText,
  MobileOverflow,
  MainText,
  MainBox,
  MainInputBox,
  MainContainer,
  MainBottomText,
  MainBottomTitle,
  MainBottomContent,
  CategoryLink,
  ChatContainerBox,
  ChatBox,
  ChatText,
  ChatIcon,
  MainMiddleBox,
  MainBottomBox,
  MiddleFlex,
  Container,
  StateBtn,
  Test,
  Logo,
  Icon,
  RankBox,
  Wrapper,
  BottomText,
  PicksterBoard,
  PicksterText,
  PicksterMain,
  PicksterRank,
  ProfileImg,
  RankText,
  RankContent,
  MainTitleBox,
  MainTopBox,
};
