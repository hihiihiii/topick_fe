import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1920];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const BoardWrapper = styled.div`
  margin: 0px auto;
  width: 1247px;
  ${mq[1]} {
    padding: 0px 67px;
    width: 634px;
  }
  ${mq[0]} {
    padding: 0px 27px;
    width: 100%;
    box-sizing: border-box;
  }
`;

const BoardBox = styled.div`
  width: 1247px;
  /* height: 921px; */
  height: 100%;
  border-radius: 7px;
  padding: 71px 149px;
  box-sizing: border-box;
  position: ${(props) => {
    if (props.contact) {
      return "";
    }
    return "relative";
  }};
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  margin-top: 20px;

  ${mq[1]} {
    width: 634px;
    padding: 49px 32px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const BoardText = styled.div`
  color: #000;
  font-size: ${(props) => {
    if (props.strap) {
      return "14px";
    }
    return "22px";
  }};
  font-weight: 600;

  ${mq[0]} {
    font-size: 16px;
  }
`;

const BoardInputBox = styled.input`
  width: 100%;
  height: 40px;
  padding-left: 14px;
  border-radius: 4px;
  border: solid 1px #ebebeb;
  background-color: #fff;
  margin-top: 10px;
  box-sizing: border-box;
`;

const BoardMarginTop = styled.div`
  margin-top: 45px;
`;

const CompletionBtn = styled.div`
  width: 131px;
  height: 45px;
  justify-self: end;
  border-radius: 5px;
  background-color: #b068f8;
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  font-size: 12px;
  font-weight: 600;
  margin-top: 36px;

  :hover {
    background-color: #f2e5ff;
  }
  ${mq[0]} {
    width: 110px;
  }
`;

const BoardFlexEnd = styled.div`
  display: flex;
  justify-content: flex-end;
`;

const BoardTextArea = styled.textarea`
  width: 100%;
  height: 432px;
  padding: 20px 20px;
  margin-top: 10px;
  box-sizing: border-box;
  border-radius: 7px;
  border: solid 1px #ebebeb;
  resize: none;
`;

const DetailTop = styled.div`
  display: flex;
  flex-direction: column;
`;
const DetailTopText = styled.div`
  font-size: ${(props) => {
    if (props.date) {
      return "18px";
    }
    return "22px";
  }};

  font-weight: ${(props) => {
    if (props.date) {
      return "500";
    }
    return "600";
  }};
  color: #000;

  ${mq[0]} {
    font-size: ${(props) => {
      if (props.date) {
        return "14px";
      }
      return "18px";
    }};
  }
`;

const DetailInfomation = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 25px;
`;

const DetailProfileImg = styled.div`
  width: 22px;
  height: 22px;
  border-radius: 999px;
  background-color: #f7f7f7;
  margin-right: 10px;
`;

const InfoFlex = styled.div`
  display: flex;
  align-items: center;
`;

const DetailInfoText = styled.div`
  font-size: 14px;
  font-weight: 500;
  color: #939393;
`;

const DetailContentsBox = styled.div`
  width: 974px;
  /* height: 320px; */
  margin: 50px 0 0;
  ${mq[1]} {
    width: 100%;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const DetailStapLine = styled.div`
  height: 21px;
  font-size: 18px;
  font-weight: 600;
  color: #000;
  margin-bottom: 15px;
  ${mq[0]} {
    font-size: 16px;
  }
`;

const DetailContent = styled.div`
  width: 974px;
  height: 284px;
  font-size: 16px;
  font-weight: 500;
  line-height: 1.5;
  color: #000;

  ${mq[1]} {
    width: 573px;
    height: ${(props) => {
      if (props.contact) {
        return "425px";
      }
      return "488px";
    }};
  }
  ${mq[0]} {
    width: 100%;
    height: ${(props) => {
      if (props.contact) {
        return "171px";
      }
      return "100%";
    }};
    font-size: 14px;
    overflow: scroll;
  }
`;

const PointBuyBox = styled.div`
  width: 400px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background-color: #b068f8;
  color: #fff;
  font-size: 18px;
  font-weight: 600;
  cursor: pointer;

  ${mq[0]} {
    width: 300px;
  }
`;

const PointFlexBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 20px;
`;

const PaidFlexBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  margin-top: 47px;
`;

const PaidOn = styled.div`
  width: 8px;
  height: 8px;
  border-radius: 999px;
  background-color: #b975fd;
  position: absolute;
  top: 6px;
  left: 6px;
`;

const PaidCard = styled.div`
  width: 48.5%;
  height: 56px;
  border-radius: 4px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  padding: 0px 26px;
  box-sizing: border-box;
  background-color: #fff;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  position: relative;
  cursor: pointer;

  ${mq[0]} {
    flex-direction: ${(props) => {
      if (props.mobile) {
        return "row";
      }
      return "column";
    }};
    width: 100%;
    margin-top: 10px;
  }
`;

const PaidText = styled.div`
  color: #000;
  font-size: ${(props) => {
    if (props.bold) {
      return "18px";
    }
    return "16px";
  }};
  font-weight: ${(props) => {
    if (props.bold) {
      return "600";
    }
    return "300";
  }};
`;

const PaidContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 15px;

  ${mq[0]} {
    flex-direction: column;
    width: 100%;
  }
`;

export {
  PaidContainer,
  PaidText,
  PaidOn,
  PaidCard,
  PaidFlexBox,
  PointFlexBox,
  DetailContentsBox,
  DetailStapLine,
  DetailContent,
  PointBuyBox,
  InfoFlex,
  DetailInfomation,
  DetailProfileImg,
  DetailInfoText,
  DetailTop,
  DetailTopText,
  BoardTextArea,
  BoardFlexEnd,
  BoardWrapper,
  BoardBox,
  BoardText,
  BoardInputBox,
  BoardMarginTop,
  CompletionBtn,
};
