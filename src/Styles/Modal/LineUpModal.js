import styled from "@emotion/styled";

const breakpoints = [450, 768, 1200, 1400, 375];

const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const LineUpBox = styled.div`
  width: 989px;
  height: 981px;
  padding: 75px 65px;
  box-sizing: border-box;
  border-radius: 5px;
  background-color: #fff;

  ${mq[1]} {
    width: 592px;
  }
  ${mq[0]} {
    width: 380px;
    height: 800px;
    padding: 0px;
  }
  ${mq[4]} {
    width: 320px;
  }
`;

const LineUpTitle = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #000;
`;

const LineTitleBox = styled.div`
  width: 100%;
  height: 45.5px;
  border-bottom: 1px solid #ededed;
`;

//라인업 팀 박스
const LineContentsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  margin-bottom: 55px;
  ${mq[1]} {
    flex-direction: column;
  }
  ${mq[0]} {
    width: fit-content;
  }
`;

//라인업 팀별 스쿼드
const LineUpTeamBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 402.5px;
  ${mq[0]} {
    width: fit-content;
  }
`;

const LineThead = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 20px;
`;

const LineTbody = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 15px;
`;

//라인업 팀로고 및 국가
const LineUpLogo = styled.div`
  width: ${(props) => {
    if (props.country) {
      return "17px";
    }
    return "21px";
  }};
  height: ${(props) => {
    if (props.country) {
      return "12px";
    }
    return "29px";
  }};
  margin-right: 15px;

  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
`;

//텍스트 및 소제목
const LineUpText = styled.div`
  font-size: ${(props) => {
    if (props.stapline) {
      return "11px";
    }
    if (props.name) {
      return "18px";
    }
    return "14px";
  }};
  font-weight: ${(props) => {
    if (props.stapline) {
      return "300";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.stapline) {
      return "#939393";
    }
    return "#000";
  }};
  ${mq[0]} {
    font-size: 10px;
  }
`;

//부상 박스
const Injury = styled.div`
  width: 45px;
  height: 20px;
  border-radius: 3px;
  font-size: 10px;
  font-weight: 600;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #ff4b4b;
  background-color: #ffeaea;
`;

const SquadImg = styled.img`
  width: 100%;
  height: 407px;
  margin-top: 41px;
  box-sizing: border-box;
  ${mq[1]} {
    width: fit-content;
  }
`;

const OverFlow = styled.div`
  overflow-x: scroll;
`;

export {
  OverFlow,
  SquadImg,
  LineUpBox,
  LineUpTitle,
  LineTitleBox,
  Injury,
  LineContentsContainer,
  LineUpTeamBox,
  LineThead,
  LineTbody,
  LineUpLogo,
  LineUpText,
};
