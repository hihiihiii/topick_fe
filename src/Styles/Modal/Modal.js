import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1400, 375];
const mq = breakpoint.map((bp) => `@media (max-width:${bp}px)`);

const ModalWrapper = styled.div`
  margin: 0px auto;
  width: 534px;
  padding: 50px 50px;
  box-sizing: border-box;
  height: 643px;
  border-radius: 5px;
  background-color: #fff;

  ${mq[0]} {
    width: 350px;
    padding: 25px 19px;
  }
  ${mq[4]} {
    width: 321px;
  }
`;

const ModalHeader = styled.div`
  width: 100%;
  height: 45.5px;
  border-bottom: 1px solid #ededed;
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const ModalText = styled.div`
  font-size: ${(props) => {
    if (props.title) {
      return "22px";
    }

    return "14px";
  }};
  margin-bottom: ${(props) => {
    if (props.title) {
      return "10px";
    }
  }};
  font-weight: 600;
  color: #000;

  ${mq[0]} {
    font-size: ${(props) => {
      if (props.title) {
        return "18px";
      }
      return "14px";
    }};
  }
`;

const PasswordForm = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 40.5px;
  position: relative;
`;

const PasswordInputBox = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 68px;
  margin-top: 20px;
`;

const PasswordInput = styled.input`
  width: 100%;
  height: 40px;
  border: 1px solid #ededed;
  border-radius: 4px;
  margin-top: 10px;
  padding-left: 15px;
  box-sizing: border-box;
`;

const CompletedBtn = styled.div`
  width: 100%;
  height: 50px;
  border-radius: 5px;
  background-color: #b975fd;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  font-size: 18px;
  font-weight: 600;
  margin-top: 40px;

  cursor: pointer;
`;

const TipsterWrapper = styled.div`
  width: 569px;
  height: ${(props) => {
    if (props.buy) {
      return "436px";
    }
    return "236px";
  }};
  border-radius: 4px;
  background-color: #fff;
  padding: 20px;
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  justify-content: center;

  ${mq[0]} {
    width: 321px;
    height: 358px;
    padding: 20px;
  }
`;

const TipsterBtn = styled.div`
  width: 234px;
  height: 45px;
  border-radius: 5px;
  border: solid 1px #f1e3ff;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${(props) => {
    if (props.cancle) {
      return "#fff";
    }
    return "#b068f8";
  }};
  color: ${(props) => {
    if (props.cancle) {
      return "#b068f8";
    }
    return "#fff";
  }};

  cursor: pointer;

  ${mq[0]} {
    width: 133.4px;
  }
`;

const TipsterFlex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-top: 20px;
`;

const TipsterMiddleBox = styled.div`
  margin-top: 20px;
  width: 100%;
  height: 45px;
  border: 1px solid #ededed;
  background-color: #fff;
  padding-left: 13px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  border-radius: 5px;
`;

const TipsterBar = styled.div`
  width: 100%;
  height: 1px;
  margin-top: 20px;
  background-color: #ededed;
`;

const TipsterText = styled.div`
  font-size: 16px;
  font-weight: 500px;
  color: #b068f8;
`;

const TipsterImg = styled.div`
  width: 28px;
  height: 28px;
  margin-right: 10px;
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
`;

const ExcahangeContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 15px;
`;

const BankSelectBox = styled.select`
  width: 100%;
  height: 40px;
  padding-left: 17px;
  box-sizing: border-box;
  border: solid 1px #ebebeb;
  border-radius: 5px;
  background-color: #fff;
  margin-top: 10px;

  -webkit-appearance: none; /* 화살표 없애기 for chrome*/
  -moz-appearance: none; /* 화살표 없애기 for firefox*/
  appearance: none; /* 화살표 없애기 공통*/
`;

const ExcahangePoint = styled.div`
  width: 100%;
  height: 40px;
  border: 1px solid #ededed;
  box-sizing: border-box;
  margin-top: 10px;
  border-radius: 5px;
  background-color: #fff;
  display: flex;
  align-items: center;
  padding-left: 14px;
`;

const WarningText = styled.div`
  margin-top: 10px;
  font-size: 11px;
  font-weight: 600;
  color: #b975fd;
`;

const BottomContainer = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 10px;

  width: 100%;
`;

const ConsentBox = styled.div`
  width: 100%;
  height: 225px;
  padding: 25px 30px;
  box-sizing: border-box;
  border-radius: 4px;
  border: solid 1px #ebebeb;
  background-color: #fcfcfc;
  margin-top: 20px;
  overflow-y: scroll;
  ::-webkit-scrollbar {
    display: none;
  }
`;

const ConsentText = styled.div`
  font-size: 12px;
  font-weight: 500;
  line-height: 20px;
  color: #333;
`;

const AcceptFlex = styled.div`
  display: flex;
  align-items: center;
  margin-top: 20px;
`;

const AcceptBox = styled.div`
  width: 24px;
  height: 24px;
  margin-right: 10px;
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
`;

const BottomArrowImg = styled.img`
  width: 12px;
  height: 7px;
  object-fit: contain;
`;
export {
  BottomArrowImg,
  AcceptFlex,
  AcceptBox,
  BottomContainer,
  ConsentText,
  ConsentBox,
  WarningText,
  ExcahangePoint,
  BankSelectBox,
  ExcahangeContainer,
  TipsterBar,
  TipsterMiddleBox,
  TipsterImg,
  TipsterText,
  TipsterFlex,
  TipsterBtn,
  TipsterWrapper,
  CompletedBtn,
  ModalWrapper,
  ModalHeader,
  ModalText,
  PasswordForm,
  PasswordInputBox,
  PasswordInput,
};
