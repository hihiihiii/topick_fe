import styled from "@emotion/styled";

const breakpoint = [450, 768, 1200, 1400, 1600];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const PointWrapper = styled.div`
  margin: 0px auto;
  width: 800.5px;

  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
    padding: 0px 24px;
    box-sizing: border-box;
  }
`;

const PointContainer = styled.div`
  margin-top: 63px;
`;

const PointHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  height: 51.5px;
  border-bottom: 1px solid #ededed;
`;

const PointColumn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const PointText = styled.div`
  font-size: ${(props) => {
    if (props.sub) {
      return "11px";
    }
    if (props.point) {
      return "20px";
    }
    return "22px";
  }};
  font-weight: ${(props) => {
    if (props.sub) {
      return "500";
    }
    return "600";
  }};
  color: ${(props) => {
    if (props.sub) {
      return "#b7b7b7";
    }
    return "#000";
  }};
`;

const PointWrapBox = styled.div`
  margin-top: 44.5px;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const PointBox = styled.div`
  width: 255px;
  height: 148.8px;
  border-radius: 7px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  margin-bottom: 17.5px;

  ${mq[1]} {
    width: 307.5px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const PointPay = styled.div`
  width: 400px;
  height: 50px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 5px;
  background-color: #b068f8;
  font-size: 18px;
  font-weight: 600;
  color: #fff;
  :hover {
    background-color: #f2e5ff;
  }
`;

const PointBoxCenter = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-top: 76.5px;
  cursor: pointer;
`;

export {
  PointBoxCenter,
  PointWrapper,
  PointContainer,
  PointHeader,
  PointColumn,
  PointText,
  PointWrapBox,
  PointPay,
  PointBox,
};
