import styled from "@emotion/styled";

const breakpoints = [450, 768, 992, 1200, 1425];
const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const BoardWrapper = styled.div`
  margin: 0px auto;
  width: 910px;
  ${mq[1]} {
    width: 634px;
  }
  /* ${mq[0]} {
    width: 321px;
  } */
  ${mq[0]} {
    width: 100%;
    box-sizing: border-box;
    padding: 0px 24px;
  }
`;

const BoardTitle = styled.div`
  font-size: 22px;
  font-weight: 600;
  color: #000;
  margin-bottom: 32px;
  ${mq[0]} {
    font-size: 22px;
  }
`;

const BoardContainer = styled.div`
  margin-top: 40px;
  ${mq[1]} {
    margin-top: 50px;
  }
  ${mq[0]} {
    margin-top: 35px;
  }
`;

const BoardImgBox = styled.div`
  width: 910px;
  height: 279px;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  background-color: #fff;
  border-radius: 16px;
  margin-bottom: 21px;
  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
  }
`;

const BoardCategoryContainer = styled.div`
  margin-top: 20px;
  width: 100%;
  height: 79px;

  display: flex;
  flex-direction: column;
`;

const CategoryTitle = styled.div`
  font-size: 20px;
  font-weight: 600;
  margin-top: 10px;
  color: #000;
  margin-left: 10px;
  ${mq[0]} {
    font-size: 20px;
  }
`;

const BoardCountBox = styled.div`
  width: 107px;
  height: 20px;
  margin-top: 45px;
  margin-left: 10px;
  font-size: 16px;
  font-weight: 500;
  color: #000;
`;

const OverFlowX = styled.div`
  ${mq[1]} {
    width: 100%;
    overflow-x: scroll;
    ::-webkit-scrollbar {
      display: none;
    }
  }
  ${mq[0]} {
    width: 100%;
    overflow-x: scroll;
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;

export {
  OverFlowX,
  BoardCountBox,
  BoardWrapper,
  BoardTitle,
  BoardContainer,
  BoardImgBox,
  BoardCategoryContainer,
  CategoryTitle,
};
