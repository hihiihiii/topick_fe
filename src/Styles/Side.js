import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const breakpoint = [375, 768, 1200, 1920];
const mq = breakpoint.map((bp) => `@media (max-width: ${bp}px)`);

const SideWrrapper = styled.div`
  width: 200px;
  margin-left: 140px;

  ${mq[1]} {
    display: ${(props) => {
      if (props.mobile) {
        return "flex";
      }
      return "none";
    }};
  }
  width: ${(props) => {
    if (props.mobile) {
      return "100%";
    }
  }};
  margin-left: ${(props) => {
    if (props.mobile) {
      return "0px";
    }
  }};
`;

const SideContainer = styled.div`
  width: 200px;
  margin-top: 40px;
  ${mq[1]} {
    width: 100%;
  }
`;

const SideBox = styled.div`
  width: 200px;
  display: flex;
  flex-direction: column;
  align-items: center;
  border: solid 1px #f5f5f5;
  ${mq[1]} {
    width: 100%;
    align-items: flex-start;
  }
`;

const SideSelectBox = styled.div`
  display: flex;
  align-items: center;
  font-weight: 600;
  padding-left: 25px;
  box-sizing: border-box;

  border-left: ${(props) => {
    if (props.action) {
      return "4px solid #b975fd";
    }
    return "";
  }};
  font-size: ${(props) => {
    if (props.title) {
      return "18px";
    }
    return "16px";
  }};
  width: 200px;
  height: 55px;
  background-color: ${(props) => {
    if (props.action) {
      return "#efdeff";
    }
    return "#fff";
  }};
  color: ${(props) => {
    if (props.action) {
      return "#b975fd";
    }
    if (props.title) {
      return "#000";
    }
    return "#a0a0a0";
  }};
  cursor: ${(props) => {
    if (props.title) {
      return "";
    }
    return "pointer";
  }};

  :hover {
    background-color: ${(props) => {
      if (props.title) {
        return "";
      }
      return "#efdeff";
    }};
    color: ${(props) => {
      if (props.title) {
        return "";
      }
      return "#b975fd";
    }};
  }
  ${mq[1]} {
    width: 100%;
  }
`;

const Linked = styled(Link)`
  text-decoration: none;
  color: #000;

  width: ${(props) => {
    if (props.more) {
      return "";
    }
    return "100%";
  }};
`;

const SideLoginBtn = styled.div`
  width: 96%;
  height: 40px;
  color: ${(props) => {
    if (props.register) {
      return "#fff";
    }
    return "#b975fd";
  }};
  border: 1px solid #b975fd;
  background-color: ${(props) => {
    if (props.register) {
      return "#b975fd";
    }
    return "#fff";
  }};
  display: flex;
  align-items: center;
  justify-content: center;
  font-weight: 600;
  font-size: 16px;
  cursor: pointer;
  border-radius: 5px;

  :hover {
    background-color: #b975fd;
    color: #fff;
  }
  ${mq[0]} {
    width: 92%;
  }
`;

const SideLoginFlex = styled.div`
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const SideProfileImg = styled.div`
  width: 26px;
  height: 26px;
  border-radius: 999px;
  margin-right: 10px;
  background-color: #f8f8f8;
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
`;

const SideProfileBox = styled.div`
  display: flex;
  align-items: center;

  margin-left: 15px;
`;

const SideProfileText = styled.div`
  font-size: 16px;
  color: #000;
  font-weight: 600;
`;

const LoginBox = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 20px;
`;

const LogoutBtn = styled.div`
  font-size: 14px;
  font-weight: 600;
  color: #000;
  text-decoration: underline;
  cursor: pointer;
`;

export {
  LoginBox,
  LogoutBtn,
  SideProfileImg,
  SideProfileBox,
  SideProfileText,
  SideContainer,
  SideBox,
  SideSelectBox,
  SideWrrapper,
  Linked,
  SideLoginBtn,
  SideLoginFlex,
};
