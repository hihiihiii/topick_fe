import styled from "@emotion/styled";
import { Link } from "react-router-dom";

const breakpoints = [470, 768, 992, 1200, 1480]; // 0 1 번쨰사용
const mq = breakpoints.map((bp) => `@media (max-width: ${bp}px)`);

const HeaderContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin: 0px 275px;
  ${mq[1]} {
    margin: 0px 25px;
  }
  ${mq[0]} {
    margin: 0px 25px;
  }
`;

const HeaderBox = styled.div`
  background-color: #fff;
  width: 100%;
  height: 86px;
  display: flex;
  justify-content: space-between;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
`;
const HeaderBoxright = styled.div`
  display: flex;
  align-items: center;
  ${mq[1]} {
    display: none;
  }
`;

const HeaderBox_Text = styled.div`
  font-size: ${(props) => {
    if (props.font) {
      return "19px";
    }
    return "16px";
  }};
  width: ${(props) => {
    if (props.action) {
      return "100px";
    }
    return "125px";
  }};
  text-align: center;
  height: 30px;
  font-weight: bold;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  font-family: "Montserrat";
`;

const HeaderBoxleft = styled.div`
  color: #b975fd;
  font-size: 24px;
  height: 30px;
  display: flex;
  align-items: center;
  justify-content: center;
  margin-bottom: ${(props) => {
    if (props.mobile) {
      return "15px";
    }
  }};
`;

const HeaderNavigation = styled(Link)`
  text-decoration: none;

  color: ${(props) => {
    if (props.action) {
      return "rgba(21, 2, 126)";
    } else {
      return "#7e6ed9";
    }
  }};
  :hover {
    color: rgba(21, 2, 126);
  }
  opacity: ${(props) => {
    if (props.action) {
      return "0.8";
    }
    return "1";
  }};
`;

const HeaderImgFlexBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const HeaderImg = styled.img`
  width: ${(props) => {
    if (props.mobile) {
      return "24px";
    }
    return "26px";
  }};
  height: 36px;
  object-fit: contain;
`;

const HeaderMiddleBox = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  ${mq[4]} {
    overflow-x: scroll;
    margin: 0px 20px;
    ::-webkit-scrollbar {
      display: none;
    }
    /* ::-webkit-scrollbar-thumb {
      background-color: #2f3542;
    } */
  }
  ${mq[1]} {
    display: none;
  }
`;
const MenuBar = styled.div`
  display: none;
  ${mq[0]} {
    width: 32px;
    height: 32px;
    background-image: ${(props) => {
      if (props.url) {
        return `url(${props.url})`;
      }
    }};
  }
`;

const MobileMenu = styled.div`
  display: none;
  cursor: pointer;
  ${mq[1]} {
    display: block;
  }
`;

const ProfileName = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: #000;
  font-family: "Arial";
`;

const WebProfileFlex = styled(Link)`
  display: flex;
  align-items: center;
  justify-content: space-between;
  text-decoration: none;
`;

const NonLoginBox = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  width: 125px;
  ${mq[1]} {
    display: none;
  }
`;

const NonLoginBar = styled.div`
  width: 2.2px;
  height: 20px;
  background-color: #b068f8;
`;

const NonLoginText = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: #b068f8;
  :hover {
    color: #000;
  }
`;

export {
  NonLoginBox,
  NonLoginBar,
  NonLoginText,
  WebProfileFlex,
  ProfileName,
  MobileMenu,
  MenuBar,
  HeaderImgFlexBox,
  HeaderMiddleBox,
  HeaderBox,
  HeaderBoxright,
  HeaderBoxleft,
  HeaderBox_Text,
  HeaderContainer,
  HeaderNavigation,
  HeaderImg,
};
