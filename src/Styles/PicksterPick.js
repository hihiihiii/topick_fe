import styled from "@emotion/styled";
import { Linked } from "./Side";

const breakpoints = [450, 768, 992, 1200, 1425];
const mq = breakpoints.map((bp) => `@media (max-width : ${bp}px)`);

const PickWrapper = styled.div`
  width: 1000px;
  margin: 0px auto;

  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
    padding: 0px 24px;
    box-sizing: border-box;
  }
`;
const PickContainer = styled.div`
  /* margin-top: 27px; */
`;

//pick box
const PickBox = styled.div`
  width: 100%;
  height: 214px;
  padding: 49px 67px;
  box-sizing: border-box;
  border-radius: 30px;
  background-color: #fff;

  /* box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16); */
  box-shadow: 13px 13px 10px rgba(0, 0, 0, 0.2);
  /* box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16); */

  margin-bottom: 20px;

  /* -webkit-filter: blur(4px);
  filter: blur(4px); */

  :hover {
    background-color: #ededed;
  }

  ${mq[1]} {
    width: 634px;
  }
  ${mq[0]} {
    width: 100%;
    height: 130px;
    padding: 16px 22.5px;
  }
`;

const PickSterTitleBox = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  position: relative;

  ${mq[1]} {
    display: none;
  }
  ${mq[0]} {
    display: none;
  }
`;

const PickSterTitle = styled.div`
  font-size: 18px;
  font-weight: 600;
  color: #000;
`;

const PickSterText = styled.div`
  font-size: ${(props) => {
    if (props.date) {
      return "11px";
    }
    return "16px";
  }};
  font-weight: ${(props) => {
    if (props.date) {
      return "";
    }
    return "600";
  }};
  font-family: ${(props) => {
    if (props.date) {
      return "Montserrat";
    }
    return "Arial";
  }};
  margin-left: ${(props) => {
    if (props.date) {
      return "10px";
    }
    return "";
  }};
  color: ${(props) => {
    if (props.date) {
      return "#aaa";
    }
    return "#000";
  }};
  ${mq[0]} {
    font-size: 10px;
  }
`;

const PickSterContent = styled.div`
  width: 100%;
  font-size: 14px;
  font-weight: 400;
  color: #000;
  line-height: 1.86;
  letter-spacing: -0.28px;
  text-align: left;
  font-family: "Montserrat";
  margin-top: 20px;
  padding: 10px 25px 0px 0px;
  box-sizing: border-box;

  ${mq[0]} {
    font-size: 11px;
  }
`;

const PickSterBottom = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  /* width: 150px; */
  /* margin-top: 21px; */
  cursor: pointer;

  ${mq[0]} {
    display: none;
  }
  ${mq[1]} {
    display: none;
  }
`;

const PickSterProfileImg = styled.div`
  width: 24px;
  height: 24px;
  margin-right: 7px;
  border-radius: 999px;
  background-color: #f7f7f7;

  ${mq[1]} {
    width: 30px;
    height: 30px;
  }
  ${mq[0]} {
    width: 30px;
    height: 30px;
  }
`;

const PicksterDrop = styled.div`
  display: flex;
  flex-direction: column;
  border-radius: 10px;
  width: 136px;
  margin-top: 160px;
  margin-left: 110px;
  border-radius: 100px;
  position: absolute;

  ${mq[0]} {
    display: none;
  }
`;

const PicksterDropMobile = styled.div`
  display: none;
  ${mq[0]} {
    position: absolute;
    display: flex;
    flex-direction: column;
    border-radius: 7px;
    box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
    width: 156px;
    background-color: #fff;
    z-index: 999;
  }
`;

const PickSterButton = styled.button`
  width: 100%;
  height: 44px;
  background-color: #fff;
  text-align: left;
  border: 1px solid #ededed;
  font-size: 14px;
  font-weight: 600;
  color: #000;
  cursor: pointer;
  z-index: 999;
  :hover {
    background-color: #f2e5ff;
  }
  ${mq[0]} {
  }
`;

const PicksterWriteBox = styled.div`
  flex-direction: row;
  align-items: center;
  box-shadow: 0 3px 6px 0 rgba(0, 0, 0, 0.16);
  opacity: 0.8;
  width: 100%;
  height: 57px;
  border-radius: 7px;
  padding: 32px 45px;
  box-sizing: border-box;
  background-color: #fff;
  margin-bottom: 21px;
  cursor: pointer;
  ${mq[0]} {
    display: flex;
    padding: 14px 22.5px;
  }
  ${mq[1]} {
    display: flex;
  }
  display: flex;
`;

const PicksterInputBox = styled.div`
  height: 20px;
  font-size: 16px;
  color: #505050;
  border-left: 1px solid #f4f4f4;
  margin-left: 10px;
  padding-left: 15px;
  font-weight: 500;
  margin-top: 4.5px;

  ${mq[0]} {
    font-size: 14px;
  }
`;

const PicksterMobile = styled.div`
  ${mq[1]} {
    width: 100%;
    display: flex;
    flex-direction: row;
    align-items: center;
    z-index: 999;
  }
  display: none;
`;

const PicksterMobileColumn = styled.div`
  display: flex;
  flex-direction: column;
`;

const PicksterText = styled.div`
  font-size: 14px;
  color: #000;
  font-weight: 600;
`;

const PicksterInput = styled.input`
  width: 100%;
  height: 48px;
  border-radius: 4px;
  border: solid 1px #ebebeb;
  padding-left: 18px;
  margin-top: 10px;
  box-sizing: border-box;
`;

const PicksterStapFlex = styled.div`
  display: flex;
  flex-direction: ${(props) => {
    if (props.row) {
      return "row";
    }
    return "column";
  }};
  margin-top: 45px;
  ${mq[1]} {
    width: 100%;
  }
`;

const PicksterSelect = styled.select`
  width: 305px;
  height: 40px;
  border: 1px solid #ebebeb;
  background-color: #fff;
  padding: 0px 14px;
  border-radius: 7px;
  ${mq[1]} {
    width: 100%;
    margin-bottom: 10px;
  }
`;
const PicksterOption = styled.option`
  font-size: 14px;
  font-weight: 600;
  opacity: 0.25px;
  color: #000;
`;

const PicksterSelectFlex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 10px;
  ${mq[1]} {
    flex-direction: column;
    align-items: flex-start;
  }
`;

const PicksterMiddleFlex = styled.div`
  display: flex;
  flex-direction: column;
  align-content: center;
  justify-content: space-between;
  width: 305px;
  ${mq[1]} {
    width: 100%;
  }
`;

const PicksterPointText = styled.div`
  font-size: 12px;
  font-weight: 500;
  color: #b975fd;
`;

const PicksterFlexRow = styled.div`
  display: flex;
  align-content: center;
  justify-content: space-between;
`;

const PointInputBox = styled.div`
  width: 305px;
  height: 40px;
  margin-top: 10px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 0px 12px;
  box-sizing: border-box;
  border: 1px solid #ededed;
  border-radius: 7px;
  ${mq[1]} {
    width: 100%;
  }
`;

const PointInput = styled.input`
  width: 100%;
  height: 95%;
  border: 0px;
`;

const MiddleFlex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;

  ${mq[1]} {
    flex-direction: column;
  }
`;

const PicksterMiddleText = styled.div`
  font-size: 14px;
  color: #000;
  font-weight: 600;
  height: 14px;
`;

const Input = styled.input`
  width: 100%;
  height: 40px;
  border: 1px solid #ededed;
  margin-top: 10px;
  border-radius: 7px;
  box-sizing: border-box;
  padding: 0px 12px;

  ${mq[1]} {
    margin-top: -50px;
  }
`;

const PickTable = styled.table`
  margin-top: 10px;
  width: 100%;
  border-collapse: collapse;
  ${mq[1]} {
    width: 974px;
  }
`;
const PickThead = styled.thead`
  width: 100%;
`;
const PickTh = styled.th`
  border: 1px solid #ededed;
  height: 42px;
  font-size: 14px;
  font-weight: 600;
  background-color: ${(props) => {
    if (props.th) {
      return "#ededed";
    }
    return "#f8f8f8";
  }};
`;
const PickTr = styled.tr``;
const PickTd = styled.td`
  text-align: center;
  width: 243.5px;
  height: 42px;
  border: 1px solid #ededed;
  font-size: 14px;
  font-weight: 600;
  background-color: ${(props) => {
    if (props.td) {
      return "#f8f8f8;";
    }
    return "#fff";
  }};
`;

const PickOverScroll = styled.div`
  height: 100%;
  overflow-y: scroll;

  ${mq[0]} {
    ::-webkit-scrollbar {
      display: none;
    }
  }
`;

const TestScroll = styled.div`
  ${mq[1]} {
    overflow-x: scroll;
  }
`;

const PicksterFree = styled.div`
  width: 48.5%;
  height: 40px;
  cursor: pointer;
  border-radius: 4px;
  border: solid 1px #ebebeb;
  background-color: #fff;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 600;
  box-sizing: border-box;
  color: #000; ;
`;

const PicksterFreeFlex = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 10px;
`;

const PicksterMobileFlex = styled.div`
  ${mq[1]} {
    display: flex;
    align-items: center;
  }
`;

const PicksterCompl = styled(Linked)`
  margin-top: 20px;
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;

const PicksterComplBtn = styled.div`
  width: 131px;
  height: 45px;
  border-radius: 5px;
  background-color: #b068f8;
  color: #fff;
  font-size: 16px;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const PicksterBackground = styled.div`
  width: 100%;
  height: 100vh;
  padding-top: 20px;
  opacity: 0.8;
  /* background-color: rgba(26, 0, 170, 0.5); */
  background-color: hsla(249, 100%, 33.3%, 0.5);
  position: relative;
`;

const PicksterMainText = styled.div`
  font-family: "Arial";
  font-size: 22px;
  font-weight: bold;
  color: #1a00aa;
  letter-spacing: 2.64px;
  margin-right: 10px;
`;

const HeaderIcon = styled.div`
  width: ${(props) => {
    if (props.content) {
      return "19px";
    }
    return "20px";
  }};
  height: ${(props) => {
    if (props.content) {
      return "19px";
    }
    return "20px";
  }};
  border-radius: 999px;
  background-color: #404040;
  opacity: 0.4;
  margin-right: 13px;
  /* margin-bottom: 1px; */
`;

const HeaderTitleFlex = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

const DropIcon = styled.img`
  width: 1.5px;
  height: 12px;
  /* margin-left: 15px;
  margin-bottom: 1px; */
  margin: 0px 0px 4px 17px;
`;

const BackgroundFlex = styled.div`
  display: flex;
  align-items: center;
  position: absolute;
`;

const BackgoundImg = styled.div`
  background-image: ${(props) => {
    if (props.url) {
      return `url(${props.url})`;
    }
  }};
  background-size: cover;
  width: 200px;
  z-index: 999;
  height: 1000px;
`;

export {
  BackgroundFlex,
  BackgoundImg,
  DropIcon,
  HeaderTitleFlex,
  HeaderIcon,
  PicksterMainText,
  PicksterBackground,
  PicksterCompl,
  PicksterComplBtn,
  TestScroll,
  PicksterMobileFlex,
  PicksterFreeFlex,
  PicksterFree,
  PickOverScroll,
  PickTable,
  PickThead,
  PickTh,
  PickTr,
  PickTd,
  Input,
  PicksterMiddleText,
  MiddleFlex,
  PointInputBox,
  PointInput,
  PicksterFlexRow,
  PicksterPointText,
  PicksterMiddleFlex,
  PicksterSelectFlex,
  PicksterOption,
  PicksterSelect,
  PicksterStapFlex,
  PicksterText,
  PicksterInput,
  PicksterDropMobile,
  PicksterMobileColumn,
  PicksterMobile,
  PicksterWriteBox,
  PicksterInputBox,
  PicksterDrop,
  PickSterButton,
  PickContainer,
  PickWrapper,
  PickBox,
  PickSterTitleBox,
  PickSterTitle,
  PickSterText,
  PickSterContent,
  PickSterBottom,
  PickSterProfileImg,
};
