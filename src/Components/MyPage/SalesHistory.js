import React, { useState } from "react";
import { PointImg } from "../../Styles/Mypage/Point";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import {
  PurchaseBox,
  PurchaseContainer,
  PurchaseDay,
  PurchaseFlex,
  PurchaseRow,
  PurchaseText,
} from "../../Styles/Mypage/Purchase";

const SalesHistory = () => {
  const [data, setData] = useState([{}, {}, {}]);
  return (
    <Inner>
      <ProfileHeader>
        <ProfileTitle>판매내역</ProfileTitle>
      </ProfileHeader>
      <PurchaseContainer>
        {data?.map((el) => {
          return (
            <>
              <PurchaseBox full>
                <PurchaseRow>
                  <PointImg history></PointImg>
                  <PurchaseFlex start>
                    <PurchaseText>dlduddls님에게 판매</PurchaseText>
                    <PurchaseText date>[축구] 팁스터 개꿀팁!</PurchaseText>
                  </PurchaseFlex>
                </PurchaseRow>
                <PurchaseFlex>
                  <PurchaseText>1,000P</PurchaseText>
                  <PurchaseText date>2021.09.01 14:21</PurchaseText>
                </PurchaseFlex>
              </PurchaseBox>
            </>
          );
        })}
      </PurchaseContainer>
    </Inner>
  );
};

export default SalesHistory;
