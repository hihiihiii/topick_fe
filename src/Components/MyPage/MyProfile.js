import React, { useState } from "react";

import {
  Inner,
  ProfileAddBox,
  ProfileFlex,
  ProfileHeader,
  ProfileImgBox,
  ProfileInfomationBox,
  ProfileInput,
  ProfileInputBox,
  ProfileStapline,
  ProfileTitle,
  ProfileImg,
  ProfileDivBox,
} from "../../Styles/Mypage/Profile";

import CustomModal from "../Modal";
import AccountChange from "../Modal/Block/AccountChange";
import PasswordChange from "../Modal/Block/PasswordChange";

const MyProfile = () => {
  const [isOpen, setIsOpen] = useState(false);
  const [isAccount, setIsAccount] = useState(false);
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        Content={() => <PasswordChange setIsOpen={setIsOpen} />}
      />
      <CustomModal
        isOpen={isAccount}
        setIsOpen={setIsAccount}
        Content={() => <AccountChange setIsOpen={setIsAccount} />}
      />
      <Inner>
        <ProfileHeader>
          <ProfileTitle>내 프로필 정보</ProfileTitle>
        </ProfileHeader>
        <ProfileFlex>
          <ProfileInfomationBox>
            <ProfileStapline>이메일</ProfileStapline>
            <ProfileInput placeholder="이메일을 입력해주세요"></ProfileInput>
            <ProfileStapline>닉네임</ProfileStapline>
            <ProfileInput placeholder="닉네임을 입력해주세요"></ProfileInput>
            <ProfileStapline>실명인증</ProfileStapline>
            <ProfileInputBox>
              <ProfileInput
                box
                placeholder="실명을 입력해주세요"
              ></ProfileInput>
              <ProfileStapline approval>인증하기</ProfileStapline>
            </ProfileInputBox>
            <ProfileStapline>비밀번호</ProfileStapline>
            <ProfileDivBox
              onClick={(e) => {
                setIsOpen(true);
              }}
            >
              {false ? "" : "비밀번호를 입력해주세요"}
            </ProfileDivBox>
            <ProfileStapline>계좌번호</ProfileStapline>
            <ProfileDivBox
              onClick={(e) => {
                setIsAccount(true);
              }}
            >
              {false ? "" : "계좌번호를 입력해주세요"}
              <ProfileStapline approval>등록하기</ProfileStapline>
            </ProfileDivBox>
          </ProfileInfomationBox>
          <ProfileImgBox>
            <ProfileStapline mobile>프로필 이미지 수정</ProfileStapline>
            <ProfileImg>
              <ProfileAddBox></ProfileAddBox>
            </ProfileImg>
          </ProfileImgBox>
        </ProfileFlex>
      </Inner>
    </>
  );
};

export default MyProfile;
