import React, { useState } from "react";
import {
  HeaderBox,
  PayBottomBox,
  PayBox,
  PayContentBox,
  PayInnerRow,
  PayInnerText,
  PayMiddleBox,
  PayOnline,
  PayText,
  PayWrpper,
} from "../../Styles/Mypage/Pay";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import CustomModal from "../Modal";
import PicksterApplication from "../Modal/Block/PicksterApplication";

const PayPickster = () => {
  const [data, setData] = useState([{}, {}, {}, {}, {}, {}]);
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        Content={() => <PicksterApplication setIsOpen={setIsOpen} />}
      />
      <Inner>
        <ProfileHeader>
          <ProfileTitle>이영인님 !</ProfileTitle>
          <HeaderBox
            onClick={() => {
              setIsOpen(true);
            }}
          >
            유료픽스터 신청
          </HeaderBox>
        </ProfileHeader>

        <PayMiddleBox>
          <PayOnline></PayOnline>
          <PayContentBox>
            <PayText>유료 픽스터 조건</PayText>
            <PayText sub>
              적중된 팁 10개 이상(2개 조합팁은 1개 팁으로 계산) + Profit
              플러스인 팁스터만 신청 가능합니다. <br />
              (실명 인증, 계좌 입력한 팁스터만 가능)
            </PayText>
          </PayContentBox>
        </PayMiddleBox>
        <PayBottomBox>
          <PayContentBox>
            <PayText bottom>총 {data?.length}개 게시물</PayText>
          </PayContentBox>
          <PayWrpper>
            {data?.map((el) => {
              return (
                <>
                  <PayBox>
                    <PayInnerText title>승률 100% 이번주 예측</PayInnerText>
                    <PayInnerText>
                      있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과
                      튼튼하며
                    </PayInnerText>
                    <PayInnerRow>
                      <PayInnerText bottom>이영인</PayInnerText>
                      <PayInnerText bottom>2021.09.10</PayInnerText>
                    </PayInnerRow>
                  </PayBox>
                </>
              );
            })}
          </PayWrpper>
        </PayBottomBox>
      </Inner>
    </>
  );
};

export default PayPickster;
