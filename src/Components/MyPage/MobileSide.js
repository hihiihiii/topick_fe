import React, { useState } from "react";
import { Logo_Main, Profile } from "../../Assets/file";
import { LogoImg } from "../../Styles/Global";
import { HeaderBoxleft } from "../../Styles/Header";

import {
  Linked,
  LoginBox,
  LogoutBtn,
  SideBox,
  SideContainer,
  SideLoginBtn,
  SideLoginFlex,
  SideProfileBox,
  SideProfileImg,
  SideProfileText,
  SideSelectBox,
  SideWrrapper,
} from "../../Styles/Side";

const MobileSide = ({ setIsOpen }) => {
  if (
    window.location.pathname === "/profile/myprofile" ||
    window.location.pathname === "/profile/point" ||
    window.location.pathname === "/profile/myblog" ||
    window.location.pathname === "/profile/paypickster" ||
    window.location.pathname === "/profile/saleshistory" ||
    window.location.pathname === "/profile/purchasehistroy" ||
    window.location.pathname === "/profile/contactdetails"
  ) {
    return (
      <>
        <SideWrrapper mobile>
          <SideContainer>
            <Linked
              to="/"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              <HeaderBoxleft mobile>
                <LogoImg main src={Logo_Main} />
              </HeaderBoxleft>
            </Linked>
            <LoginBox>
              <Linked
                more
                to="/profile/myprofile"
                onClick={() => {
                  setIsOpen(false);
                }}
              >
                <SideProfileBox>
                  <SideProfileImg url={Profile}></SideProfileImg>
                  <SideProfileText>hihihi</SideProfileText>
                </SideProfileBox>
              </Linked>
              <LogoutBtn>로그아웃</LogoutBtn>
            </LoginBox>
            <SideBox>
              <SideSelectBox title>프로필 설정</SideSelectBox>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/myprofile"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/myprofile"
                      ? true
                      : false
                  }
                >
                  내 프로필
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/point"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/point" ? true : false
                  }
                >
                  포인트
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/purchasehistroy"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/purchasehistroy"
                      ? true
                      : false
                  }
                >
                  구매내역
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/saleshistory"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/saleshistory"
                      ? true
                      : false
                  }
                >
                  판매내역
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/paypickster"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/paypickster"
                      ? true
                      : false
                  }
                >
                  유료 픽스터
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/contactdetails"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/contactdetails"
                      ? true
                      : false
                  }
                >
                  문의 내역
                </SideSelectBox>
              </Linked>
              <Linked
                onClick={() => {
                  setIsOpen(false);
                }}
                to="/profile/myblog"
              >
                <SideSelectBox
                  action={
                    window.location.pathname === "/profile/myblog"
                      ? true
                      : false
                  }
                >
                  내 블로그
                </SideSelectBox>
              </Linked>
            </SideBox>
          </SideContainer>
        </SideWrrapper>
      </>
    );
  } else {
    //Profile 일때.
    return (
      <SideWrrapper mobile>
        <SideContainer>
          <Linked
            to="/"
            onClick={() => {
              setIsOpen(false);
            }}
          >
            <HeaderBoxleft mobile>
              <LogoImg main src={Logo_Main} />
            </HeaderBoxleft>
          </Linked>

          {/* 로그인, 회원가입 */}
          <SideLoginFlex>
            <Linked
              to="/login"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              <SideLoginBtn>로그인</SideLoginBtn>
            </Linked>
            <Linked
              to="/register"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              <SideLoginBtn register>회원가입</SideLoginBtn>
            </Linked>
          </SideLoginFlex>

          {/* 로그인 상태 */}
          {/* <LoginBox>
            <Linked
              more
              to="/profile/myprofile"
              onClick={() => {
                setIsOpen(false);
              }}
            >
              <SideProfileBox>
                <SideProfileImg url={Profile}></SideProfileImg>
                <SideProfileText>hihihi</SideProfileText>
              </SideProfileBox>
            </Linked>
            <LogoutBtn>로그아웃</LogoutBtn>
          </LoginBox> */}

          <SideBox>
            <SideSelectBox title>메인메뉴</SideSelectBox>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/pick"
            >
              <SideSelectBox
                action={window.location.pathname === "/pick" ? true : false}
              >
                픽스터 pick
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/board"
            >
              <SideSelectBox
                action={window.location.pathname === "/board" ? true : false}
              >
                공지사항
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/rank"
            >
              <SideSelectBox
                action={window.location.pathname === "/rank" ? true : false}
              >
                픽스터 랭킹
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/proto"
            >
              <SideSelectBox
                action={window.location.pathname === "/proto" ? true : false}
              >
                프로토
              </SideSelectBox>
            </Linked>
            {/* <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/2"
            >
              <SideSelectBox
                action={
                  window.location.pathname === "/2"
                    ? true
                    : false
                }
              >
                2부리그
              </SideSelectBox>
            </Linked> */}
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/purchaserate"
            >
              <SideSelectBox
                action={
                  window.location.pathname === "/purchaserate" ? true : false
                }
              >
                국내 구매율
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/covers"
            >
              <SideSelectBox
                action={window.location.pathname === "/covers" ? true : false}
              >
                커버스 픽
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/todayfact"
            >
              <SideSelectBox
                action={
                  window.location.pathname === "/todayfact" ? true : false
                }
              >
                오늘의 팩트체크
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/lineup"
            >
              <SideSelectBox
                action={window.location.pathname === "/lineup" ? true : false}
              >
                라인업
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/dividenddroprate"
            >
              <SideSelectBox
                action={
                  window.location.pathname === "/dividenddroprate"
                    ? true
                    : false
                }
              >
                배당 하락율
              </SideSelectBox>
            </Linked>
            <Linked
              onClick={() => {
                setIsOpen(false);
              }}
              to="/profile/myprofile"
            >
              <SideSelectBox
                action={
                  window.location.pathname === "/profile/myprofile"
                    ? true
                    : false
                }
              >
                마이페이지
              </SideSelectBox>
            </Linked>
          </SideBox>
        </SideContainer>
      </SideWrrapper>
    );
  }
};

export default MobileSide;
