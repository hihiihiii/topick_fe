import React, { useState } from "react";
import {
  FlexBox,
  PointBox,
  PointFlexColumn,
  PointHistory,
  PointHistoryBox,
  PointImg,
  PointMain,
  PointText,
  PointTitleFlexBox,
} from "../../Styles/Mypage/Point";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import { Linked } from "../../Styles/Side";
import CustomModal from "../Modal";
import PointExchange from "../Modal/Block/PointExchange";

const Point = () => {
  const [type, setType] = useState(1);
  const [data, setData] = useState([{}, {}, {}]);
  const [isOpen, setIsOpen] = useState(false);
  return (
    <>
      <CustomModal
        isOpen={isOpen}
        setIsOpen={setIsOpen}
        Content={() => (
          <PointExchange setIsOpen={setIsOpen} setType={setType} />
        )}
      />
      <Inner>
        <ProfileHeader>
          <ProfileTitle>포인트</ProfileTitle>
          <PointTitleFlexBox>
            <Linked more to="/pointcharge">
              <PointBox
                action={type === 1 ? true : false}
                onClick={() => {
                  setType(1);
                }}
              >
                충전하기
              </PointBox>
            </Linked>
            <PointBox
              action={type === 2 ? true : false}
              onClick={() => {
                setType(2);
                setIsOpen(true);
              }}
            >
              환급하기
            </PointBox>
          </PointTitleFlexBox>
        </ProfileHeader>

        <PointMain>
          <PointFlexColumn>
            <PointText>15,000P</PointText>
            <PointText sub>보유 포인트</PointText>
          </PointFlexColumn>

          <PointHistoryBox>
            <PointText>포인트 내역</PointText>
            {data?.map((el, idx) => {
              return (
                <PointHistory>
                  <FlexBox>
                    <PointImg></PointImg>
                    <PointFlexColumn>
                      <PointText point>포인트 지급</PointText>
                      <PointText sub>2021.09.01 14:21</PointText>
                    </PointFlexColumn>
                  </FlexBox>
                  <PointText point>+1,000P</PointText>
                </PointHistory>
              );
            })}
          </PointHistoryBox>
        </PointMain>
      </Inner>
    </>
  );
};

export default Point;
