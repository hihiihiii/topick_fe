import React, { useState } from "react";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import Chart from "react-apexcharts";

import { PointTitleFlexBox, PointBox } from "../../Styles/Mypage/Point";
import {
  BlogBottom,
  BlogChartBox,
  BlogCircle,
  BlogContainer,
  BlogContent,
  BlogInner,
  BlogMiddleBox,
  BlogModi,
  BlogModiText,
  BlogOverScroll,
  BlogTable,
  BlogTbody,
  BlogTd,
  BlogText,
  BlogTh,
  BlogThead,
  BlogTop,
  BlogTopBox,
} from "../../Styles/Mypage/Blog";
import {
  PayBottomBox,
  PayBox,
  PayContentBox,
  PayInnerRow,
  PayInnerText,
  PayText,
  PayWrpper,
} from "../../Styles/Mypage/Pay";

const MyBlog = () => {
  const [type, setType] = useState(1);

  const [count, setCount] = useState([{}, {}, {}, {}, {}, {}, {}]);

  const [data, setData] = useState({
    options: {
      chart: {
        id: "area",
      },
      xaxis: {
        categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998],
      },
    },
    // dataLabels: {
    //   enabled: false,
    // },
    series: [
      {
        name: "series-1",
        data: [30, 40, 45, 50, 49, 60, 70, 91],
      },
    ],
  });
  return (
    <Inner>
      <ProfileHeader>
        <ProfileTitle>내 블로그</ProfileTitle>
        <PointTitleFlexBox>
          <PointBox
            action={type === 1 ? true : false}
            onClick={() => {
              setType(1);
            }}
          >
            팁스터 소개
          </PointBox>
          <PointBox
            action={type === 2 ? true : false}
            onClick={() => {
              setType(2);
            }}
          >
            지난글 보기
          </PointBox>
        </PointTitleFlexBox>
      </ProfileHeader>
      {type === 1 && (
        <>
          <BlogContainer>
            <BlogOverScroll>
              <BlogTop>
                <BlogTopBox>
                  <BlogInner>
                    <BlogCircle purple></BlogCircle>
                    <BlogText sub>지난 3개월 수익</BlogText>
                    <BlogText>10,000P</BlogText>
                  </BlogInner>
                </BlogTopBox>
                <BlogTopBox>
                  <BlogInner>
                    <BlogCircle green></BlogCircle>
                    <BlogText sub>지난 1주일 수익</BlogText>
                    <BlogText>510,000P</BlogText>
                  </BlogInner>
                </BlogTopBox>
                <BlogTopBox>
                  <BlogInner>
                    <BlogCircle yellow></BlogCircle>
                    <BlogText sub>투자 수익률</BlogText>
                    <BlogText>+ 51.21</BlogText>
                  </BlogInner>
                </BlogTopBox>
              </BlogTop>
            </BlogOverScroll>
            <BlogMiddleBox>
              <BlogText>프로핏 차트 (총 누적)</BlogText>
              <BlogOverScroll>
                <BlogChartBox>
                  <Chart
                    options={data?.options}
                    series={data?.series}
                    type="area"
                    width={"100%"}
                    height={"100%"}
                  />
                </BlogChartBox>
              </BlogOverScroll>
            </BlogMiddleBox>
            <BlogModi>
              <BlogText>팁스터 소개</BlogText>
              <BlogModiText>수정</BlogModiText>
            </BlogModi>
            <BlogContent>
              있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며,
              품에 때까지 듣는다. 길지 두손을 못하다 것은 있는 꽃이 듣기만 구할
              청춘의 칼이다. 찾아 이상의 방황하였으며, 석가는 운다. 내는 트고,
              간에 목숨이 얼음 것이다. 하였으며, 없으면, 그러므로 천하를 그들의
              방황하였으며, 것이다.
            </BlogContent>
            <BlogBottom>
              <BlogText>종목 개별 수익률</BlogText>
              <BlogOverScroll>
                <BlogTable>
                  <BlogThead>
                    <BlogTh style={{ width: 150 }}>종목</BlogTh>
                    <BlogTh style={{ width: 250 }}>수익</BlogTh>
                    <BlogTh style={{ width: 200 }}>투자대비수익률</BlogTh>
                    <BlogTh style={{ width: 40 }}>팁</BlogTh>
                  </BlogThead>
                  <BlogTbody>
                    <BlogTd style={{ width: 150 }}>축구</BlogTd>
                    <BlogTd style={{ width: 250 }}>+100</BlogTd>
                    <BlogTd style={{ width: 200 }}>51%</BlogTd>
                    <BlogTd style={{ width: 40 }}>500</BlogTd>
                  </BlogTbody>
                  <BlogTbody>
                    <BlogTd style={{ width: 150 }}>야구</BlogTd>
                    <BlogTd style={{ width: 250 }}>+100</BlogTd>
                    <BlogTd style={{ width: 200 }}>51%</BlogTd>
                    <BlogTd style={{ width: 40 }}>500</BlogTd>
                  </BlogTbody>
                  <BlogTbody>
                    <BlogTd style={{ width: 150 }}>농구</BlogTd>
                    <BlogTd style={{ width: 250 }}>+100</BlogTd>
                    <BlogTd style={{ width: 200 }}>51%</BlogTd>
                    <BlogTd style={{ width: 40 }}>500</BlogTd>
                  </BlogTbody>
                </BlogTable>
              </BlogOverScroll>
            </BlogBottom>
            <BlogBottom>
              <BlogText>월별 히스토리</BlogText>
              <BlogOverScroll>
                <BlogTable>
                  <BlogThead>
                    <BlogTh style={{ width: 150 }}>월</BlogTh>
                    <BlogTh style={{ width: 250 }}>수익</BlogTh>
                    <BlogTh style={{ width: 200 }}>투자대비수익률</BlogTh>
                    <BlogTh style={{ width: 40 }}>팁</BlogTh>
                  </BlogThead>
                  <BlogTbody>
                    <BlogTd style={{ width: 150 }}>2021.12</BlogTd>
                    <BlogTd style={{ width: 250 }}>+100</BlogTd>
                    <BlogTd style={{ width: 200 }}>51%</BlogTd>
                    <BlogTd style={{ width: 40 }}>500</BlogTd>
                  </BlogTbody>
                </BlogTable>
              </BlogOverScroll>
            </BlogBottom>
            <BlogBottom>
              <BlogText>종목 개별 수익률</BlogText>
              <BlogOverScroll>
                <BlogTable>
                  <BlogThead>
                    <BlogTh style={{ width: 150 }}>날짜</BlogTh>
                    <BlogTh style={{ width: 250 }}>리그</BlogTh>
                    <BlogTh style={{ width: 180 }}>홈</BlogTh>
                    <BlogTh style={{ width: 100 }}>어웨이</BlogTh>
                  </BlogThead>
                  <BlogTbody>
                    <BlogTd style={{ width: 150 }}>2021.12</BlogTd>
                    <BlogTd style={{ width: 250 }}>MLB</BlogTd>
                    <BlogTd style={{ width: 180 }}>뉴욕 양키스</BlogTd>
                    <BlogTd style={{ width: 100 }}>LA 다저스</BlogTd>
                  </BlogTbody>
                </BlogTable>
              </BlogOverScroll>
            </BlogBottom>
          </BlogContainer>
        </>
      )}

      {type === 2 && (
        <>
          <PayBottomBox>
            <PayContentBox>
              <PayText bottom>총 {count.length}개 게시물</PayText>
            </PayContentBox>
            <PayWrpper>
              {count?.map((el) => {
                return (
                  <>
                    <PayBox>
                      <PayInnerText title>승률 100% 이번주 예측</PayInnerText>
                      <PayInnerText>
                        있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과
                        튼튼하며
                      </PayInnerText>
                      <PayInnerRow>
                        <PayInnerText bottom>이영인</PayInnerText>
                        <PayInnerText bottom>2021.09.10</PayInnerText>
                      </PayInnerRow>
                    </PayBox>
                  </>
                );
              })}
            </PayWrpper>
          </PayBottomBox>
        </>
      )}
    </Inner>
  );
};
export default MyBlog;
