import React, { useState } from "react";
import { PointImg } from "../../Styles/Mypage/Point";
import {
  Inner,
  ProfileHeader,
  ProfileTitle,
} from "../../Styles/Mypage/Profile";
import {
  PurchaseBox,
  PurchaseContainer,
  PurchaseDay,
  PurchaseFlex,
  PurchaseRow,
  PurchaseText,
} from "../../Styles/Mypage/Purchase";
const PurchaseHistroy = () => {
  const [data, setData] = useState([{}, {}, {}]);
  return (
    <Inner>
      <ProfileHeader>
        <ProfileTitle>구매내역</ProfileTitle>
      </ProfileHeader>
      <PurchaseContainer>
        {data?.map((el) => {
          return (
            <>
              <PurchaseBox full>
                <PurchaseRow>
                  <PointImg history></PointImg>
                  <PurchaseText>[축구] 팁스터 개꿀팁!</PurchaseText>
                  <PurchaseDay>D-9</PurchaseDay>
                </PurchaseRow>
                <PurchaseFlex>
                  <PurchaseText>1,000P</PurchaseText>
                  <PurchaseText date>2021.09.01 14:21</PurchaseText>
                </PurchaseFlex>
              </PurchaseBox>
            </>
          );
        })}
      </PurchaseContainer>
    </Inner>
  );
};

export default PurchaseHistroy;
