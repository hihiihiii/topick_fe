import React from "react";
import { withRouter } from "react-router";
import { Message } from "../Assets/file";
import {
  ChatBox,
  ChatContainerBox,
  ChatIcon,
  ChatText,
} from "../Styles/Global";
const Chat = ({ location }) => {
  if (location.pathname === "/login" || location.pathname === "/register") {
    return <></>;
  } else {
    return (
      <>
        <ChatBox>
          <ChatIcon url={Message} style={{ marginRight: 10 }}></ChatIcon>
          <ChatText>채팅참여</ChatText>
        </ChatBox>
      </>
    );
  }
};

export default withRouter(Chat);
