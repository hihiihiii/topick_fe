import React, { useState } from "react";
import {
  CancleBtn,
  CardBox,
  CardFlexBox,
  CardText,
  GameTitleBox,
  InfoTable,
  InfoTableText,
  InfoTbody,
  InfoText,
  InfoThead,
  InfoWrapper,
  OddsBox,
  RelatedArticlesBox,
  RelatedArticlesFlex,
  RelatedFlex,
  RelatedOverFlow,
  RelatedText,
} from "../../../Styles/Modal/Infomation";

const GameInfomation = ({ setIsOpen }) => {
  const [type, setType] = useState(1);
  return (
    <>
      <InfoWrapper>
        <GameTitleBox>
          <CardText>레알 마드리드 - FC Real Madrid</CardText>
          <CancleBtn
            onClick={() => {
              setIsOpen(false);
            }}
          ></CancleBtn>
        </GameTitleBox>

        <CardFlexBox>
          <CardBox
            onClick={() => {
              setType(1);
            }}
            action={type === 1 ? true : false}
          >
            <CardText>A</CardText>
            <CardText sub>A조</CardText>
          </CardBox>
          <CardBox
            onClick={() => {
              setType(2);
            }}
            action={type === 2 ? true : false}
          >
            <CardText>B</CardText>
            <CardText sub>B조</CardText>
          </CardBox>
          <CardBox
            onClick={() => {
              setType(3);
            }}
            action={type === 3 ? true : false}
          >
            <CardText>C</CardText>
            <CardText sub>C조</CardText>
          </CardBox>
          <CardBox
            onClick={() => {
              setType(4);
            }}
            action={type === 4 ? true : false}
          >
            <CardText>D</CardText>
            <CardText sub>D조</CardText>
          </CardBox>
        </CardFlexBox>

        <RelatedOverFlow>
          <InfoTable>
            <InfoThead>
              <InfoTableText style={{ width: 120 }} th>
                경기
              </InfoTableText>
              <InfoTableText style={{ width: 120 }} th>
                시간
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                홈팀
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                결과
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                원정팀
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                승률
              </InfoTableText>
            </InfoThead>
            <InfoTbody>
              <InfoTableText style={{ width: 120 }}>경기</InfoTableText>
              <InfoTableText style={{ width: 120 }}>시간</InfoTableText>
              <InfoTableText style={{ width: 150 }}>홈팀</InfoTableText>
              <InfoTableText style={{ width: 150 }}>결과</InfoTableText>
              <InfoTableText style={{ width: 150 }}>원정팀</InfoTableText>
              <InfoTableText style={{ width: 150 }}>
                <OddsBox>패배</OddsBox>
              </InfoTableText>
            </InfoTbody>
          </InfoTable>
        </RelatedOverFlow>

        <InfoText>승무패</InfoText>
        <RelatedOverFlow>
          <InfoTable>
            <InfoThead>
              <InfoTableText style={{ width: 120 }} th>
                순위
              </InfoTableText>
              <InfoTableText style={{ width: 120 }} th>
                팀
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                경기
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                승
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                무
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                패
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                득점
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                실점
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                승점
              </InfoTableText>
              <InfoTableText style={{ width: 150 }} th>
                비고
              </InfoTableText>
            </InfoThead>
            <InfoTbody>
              <InfoTableText style={{ width: 120 }}>1</InfoTableText>
              <InfoTableText style={{ width: 120 }}>레알마드리드</InfoTableText>
              <InfoTableText style={{ width: 150 }}>5</InfoTableText>
              <InfoTableText style={{ width: 150 }}>3</InfoTableText>
              <InfoTableText style={{ width: 150 }}>2</InfoTableText>
              <InfoTableText style={{ width: 150 }}>2</InfoTableText>
              <InfoTableText style={{ width: 150 }}>5</InfoTableText>
              <InfoTableText style={{ width: 150 }}>2</InfoTableText>
              <InfoTableText style={{ width: 150 }}>3</InfoTableText>
              <InfoTableText style={{ width: 150 }}></InfoTableText>
            </InfoTbody>
          </InfoTable>
        </RelatedOverFlow>
        <InfoText>관련기사</InfoText>
        <RelatedOverFlow>
          <RelatedArticlesFlex>
            <RelatedArticlesBox>
              <RelatedFlex>
                <RelatedText>기사 제목입니다..</RelatedText>
                <RelatedText date>2021.09.01</RelatedText>
              </RelatedFlex>
              <RelatedText content>
                있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며,
                품에 때까지 듣는다…
              </RelatedText>
            </RelatedArticlesBox>
            <RelatedArticlesBox>
              <RelatedFlex>
                <RelatedText>기사 제목입니다..</RelatedText>
                <RelatedText date>2021.09.01</RelatedText>
              </RelatedFlex>
              <RelatedText content>
                있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며,
                품에 때까지 듣는다…
              </RelatedText>
            </RelatedArticlesBox>
            <RelatedArticlesBox>
              <RelatedFlex>
                <RelatedText>기사 제목입니다..</RelatedText>
                <RelatedText date>2021.09.01</RelatedText>
              </RelatedFlex>
              <RelatedText content>
                있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며,
                품에 때까지 듣는다…
              </RelatedText>
            </RelatedArticlesBox>
            <RelatedArticlesBox>
              <RelatedFlex>
                <RelatedText>기사 제목입니다..</RelatedText>
                <RelatedText date>2021.09.01</RelatedText>
              </RelatedFlex>
              <RelatedText content>
                있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며,
                품에 때까지 듣는다…
              </RelatedText>
            </RelatedArticlesBox>
          </RelatedArticlesFlex>
        </RelatedOverFlow>
      </InfoWrapper>
    </>
  );
};

export default GameInfomation;
