import React from "react";
import {
  ModalText,
  TipsterBtn,
  TipsterFlex,
  TipsterWrapper,
} from "../../../Styles/Modal/Modal";

const PicksterApplication = ({ setIsOpen }) => {
  return (
    <>
      <TipsterWrapper>
        <ModalText title>유료 픽스터 전환하기</ModalText>

        <ModalText>
          마이 페이지의 셀러 어드민에서 유료 팁스터를 신청 할 수 있습니다.
          <br />
          이동하시겠습니까 ?
        </ModalText>

        <TipsterFlex>
          <TipsterBtn
            onClick={() => {
              setIsOpen(false);
            }}
            cancle
          >
            취소
          </TipsterBtn>
          <TipsterBtn>전환하기</TipsterBtn>
        </TipsterFlex>
      </TipsterWrapper>
    </>
  );
};

export default PicksterApplication;
