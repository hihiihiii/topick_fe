import React, { useState } from "react";
import { Accept } from "../../../Assets/file";
import {
  AcceptBox,
  AcceptFlex,
  BankSelectBox,
  BottomContainer,
  CompletedBtn,
  ConsentBox,
  ConsentText,
  ExcahangeContainer,
  ExcahangePoint,
  ModalHeader,
  ModalText,
  ModalWrapper,
  PasswordInput,
  PasswordInputBox,
  WarningText,
} from "../../../Styles/Modal/Modal";

const AccountChange = ({ setIsOpen }) => {
  const [data, setData] = useState({ location: "none" });
  const [accept, setAccept] = useState(true);
  return (
    <>
      <ModalWrapper>
        <ModalHeader>
          <ModalText title>계좌번호 등록</ModalText>
          <ModalText title>1,500P</ModalText>
        </ModalHeader>
        <ExcahangeContainer>
          <PasswordInputBox>
            <ModalText>은행명</ModalText>
            <BankSelectBox
              style={{ color: data?.location === "none" ? "#a7a7a7" : "#000" }}
              onChange={(e) => {
                setData({ ...data, location: e.target.value });
              }}
            >
              <option value={"none"}>주 거래 은행을 선택해주세요</option>
              <option value={1}>우리은행</option>
              <option value={2}>하나은행</option>
              <option value={3}>국민은행</option>
            </BankSelectBox>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>계좌번호</ModalText>
            <PasswordInput placeholder="계좌번호를 입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>예금주 명</ModalText>
            <PasswordInput placeholder="예금주명를 입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>환전신청 가능액</ModalText>
            <ExcahangePoint>
              <ModalText>15,000원</ModalText>
            </ExcahangePoint>
          </PasswordInputBox>
        </ExcahangeContainer>
        <WarningText>보유 포인트가 부족합니다</WarningText>

        <BottomContainer>
          <ModalText>유의사항 / 동의사항</ModalText>
          <ConsentBox>
            <ConsentText>
              개인정보보호위원회가 취급하는 모든 개인정보는 관련 법령에 근거하여
              수집 · 보유 및 처리되고 있습니다. 「개인정보보호법」은 이러한
              개인정보의 취급에 대한 일반적 규범을 제시하고 있으며,
              개인정보보호위원회는 이러한 법령의 규정에 따라 수집 · 보유 및
              처리하는 개인정보를 공공업무의 적절한 수행과 이용자의 권익을
              보호하기 위해 적법하고 적정하게 취급할 것입니다. 또한,
              개인정보보호위원회는 관련 법령에서 규정한 바에 따라 보유하고 있는
              개인정보에 대한 열람, 정정·삭제, 처리정지 요구 등 이용자의 권익을
              존중하며, 이용자는 이러한 법령상 권익의 침해 등에 대하여
              행정심판법에서 정하는 바에 따라 행정심판을 청구할 수 있습니다.
              개인정보보호위원회는 개인정보보호법 제 30조에 따라 정보주체의
              개인정보 보호 및 권익을 보호하고 개인정보와 관련한 이용자의 고충을
              원활하게 처리할 수 있도록 다음과 같은 개인정보 처리방침을 수립 ·
              공개하고 있습니다.
            </ConsentText>
          </ConsentBox>
          <AcceptFlex>
            {accept ? (
              <AcceptBox
                url={Accept}
                onClick={() => {
                  setAccept(false);
                }}
              ></AcceptBox>
            ) : (
              <AcceptBox
                onClick={() => {
                  setAccept(true);
                }}
              ></AcceptBox>
            )}

            <ModalText>동의 하기</ModalText>
          </AcceptFlex>
        </BottomContainer>
        <CompletedBtn
          onClick={() => {
            setIsOpen(false);
          }}
        >
          완료
        </CompletedBtn>
      </ModalWrapper>
    </>
  );
};

export default AccountChange;
