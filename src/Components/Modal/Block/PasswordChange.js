import React from "react";
import {
  CompletedBtn,
  ModalHeader,
  ModalText,
  ModalWrapper,
  PasswordForm,
  PasswordInput,
  PasswordInputBox,
} from "../../../Styles/Modal/Modal";

const PasswordChange = ({ setIsOpen }) => {
  return (
    <>
      <ModalWrapper>
        <ModalHeader>
          <ModalText title>비밀번호 변경</ModalText>
        </ModalHeader>
        <PasswordForm>
          <PasswordInputBox>
            <ModalText>기존 비밀번호</ModalText>
            <PasswordInput placeholder="기존 비밀번호를 입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>새 비밀번호</ModalText>
            <PasswordInput placeholder="새 비밀번호를 입력해주세요"></PasswordInput>
          </PasswordInputBox>
          <PasswordInputBox>
            <ModalText>비밀번호 확인</ModalText>
            <PasswordInput placeholder="비밀번호를 재입력해주세요"></PasswordInput>
          </PasswordInputBox>

          <CompletedBtn
            onClick={() => {
              setIsOpen(false);
            }}
          >
            완료
          </CompletedBtn>
        </PasswordForm>
      </ModalWrapper>
    </>
  );
};

export default PasswordChange;
