import React, { useState } from "react";
import { CoversFlexBox, CoversImg, CoversText } from "../../Styles/Covers";
import { OverFlowX } from "../../Styles/PicksterBoard";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
  ResultBox,
  ResultText,
} from "../../Styles/PurchaseRate";

import { Team } from "../../Assets/file";

const Covers = () => {
  const [data, setData] = useState([{}, {}]);
  return (
    <>
      <PurchaseWrapper>
        <PurchaseContainer>
          <PurchaseTitle title>커버스픽</PurchaseTitle>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>경기번호</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>매치업</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 265 }}>시간</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>확률</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>확률</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 85 }}>픽률</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody covers>
                    <PurchaseTop_td style={{ width: 125 }}>
                      JDKFG09
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      <CoversFlexBox>
                        <CoversFlexBox row>
                          <CoversText>레알 마드리드</CoversText>
                          <CoversImg url={Team}></CoversImg>
                        </CoversFlexBox>
                        <CoversFlexBox row>
                          <CoversText>레알 마드리드</CoversText>
                          <CoversImg url={Team}></CoversImg>
                        </CoversFlexBox>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 265 }}>
                      <CoversFlexBox>
                        <CoversText date>2021.09.01 08:32</CoversText>
                        <CoversText></CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <CoversFlexBox>
                        <CoversText title>77%</CoversText>
                        <CoversText>23%</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <CoversFlexBox>
                        <CoversText>-3.5</CoversText>
                        <CoversText>-3.5</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 85 }}>
                      <CoversFlexBox>
                        <CoversText>364</CoversText>
                        <CoversText>789</CoversText>
                      </CoversFlexBox>
                    </PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default Covers;
