import React from "react";
import { Link } from "react-router-dom";
import { Baseball, Basketball, Soccer, Volleyball } from "../../Assets/file";
import {
  MainContainer,
  MainTitleBox,
  PicksterBoard,
  PicksterMain,
  PicksterText,
  ProfileImg,
  MainTopBox,
  PicksterRank,
  RankBox,
  RankText,
  RankContent,
  MainMiddleBox,
  MiddleFlex,
  Icon,
  CategoryLink,
  MainBottomBox,
  MainBottomText,
  MainBottomTitle,
  MainBottomContent,
  StateBtn,
  MobileRankIcon,
  MobilePicksterRank,
  MobileRankText,
  OverflowBox,
  MobilePicsterContent,
  FitContent,
} from "../../Styles/Global";
import { Linked } from "../../Styles/Side";

const Main = () => {
  const data = [
    { title: "[축구] 레알 마드리드 VS 레알 마드리드" },
    { title: "[축구] 레알 마드리드 VS 레알 마드리드" },
    { title: "[축구] 레알 마드리드 VS 레알 마드리드" },
    { title: "[축구] 레알 마드리드 VS 레알 마드리드" },
  ];

  const rank = [
    { name: "김승환", rank: 1, img: "" },
    { name: "김승환", rank: 2, img: "" },
    { name: "김승환", rank: 3, img: "" },
    { name: "김승환", rank: 4, img: "" },
    { name: "김승환", rank: 5, img: "" },
    { name: "김승환", rank: 6, img: "" },
  ];

  const Category = [
    { title: "축구", img: Soccer, link: "/Soccer" },
    { title: "야구", img: Baseball, link: "/Basketball" },
    { title: "농구", img: Basketball, link: "/Baseball" },
    { title: "배구", img: Volleyball, link: "/Volleyball" },
  ];

  const Game = [
    {
      game: "JPND2",
      time: "8:00",
      state: "완료",
      home: "레알마드리드",
      score: "3:2",
      away: "바르셀로나",
      half: "1 - 0",
    },
    {
      game: "JPND2",
      time: "8:00",
      state: "취소",
      home: "레알마드리드",
      score: "3:2",
      away: "바르셀로나",
      half: "1 - 0",
    },
    {
      game: "JPND2",
      time: "8:00",
      state: "진행중",
      home: "레알마드리드",
      score: "3:2",
      away: "바르셀로나",
      half: "1 - 0",
    },
    {
      game: "JPND2",
      time: "8:00",
      state: "취소",
      home: "레알마드리드",
      score: "3:2",
      away: "바르셀로나",
      half: "1 - 0",
    },
    {
      game: "JPND2",
      time: "8:00",
      state: "취소",
      home: "레알마드리드",
      score: "3:2",
      away: "바르셀로나",
      half: "1 - 0",
    },
  ];

  return (
    <>
      <MainContainer>
        <MainTopBox>
          <PicksterBoard>
            <MainTitleBox>
              <PicksterText board>픽스터 Pick</PicksterText>
              <Linked to="/pick" more>
                <PicksterText more>더보기</PicksterText>
              </Linked>
            </MainTitleBox>
            {data?.map((el, idx) => {
              return (
                <Linked more to="/pick/detail">
                  <PicksterMain style={{ marginBottom: 10 }}>
                    {el?.title}
                  </PicksterMain>
                </Linked>
              );
            })}
          </PicksterBoard>

          <PicksterBoard>
            <MainTitleBox>
              <PicksterText board>픽스터 게시판</PicksterText>
              <Linked to="/board" more>
                <PicksterText more>더보기</PicksterText>
              </Linked>
            </MainTitleBox>
            {data?.map((el, idx) => {
              return (
                <Linked to="/board/detail" more>
                  <PicksterMain style={{ marginBottom: 10 }}>
                    {el?.title}
                  </PicksterMain>
                </Linked>
              );
            })}
          </PicksterBoard>

          <PicksterBoard mobile>
            <MainTitleBox>
              <PicksterText board>픽스터 랭킹</PicksterText>
              <Linked to="/rank" more>
                <PicksterText more>더보기</PicksterText>
              </Linked>
            </MainTitleBox>
            <OverflowBox>
              <RankBox>
                {rank?.map((el, idx) => {
                  return (
                    <>
                      {/* 웹 */}
                      <PicksterRank style={{ marginBottom: 14 }}>
                        <RankContent>
                          <RankText num>{el?.rank}</RankText>
                          <ProfileImg></ProfileImg>
                          <RankText>{el?.name}</RankText>
                        </RankContent>
                      </PicksterRank>

                      {/* 모바일 */}
                      <MobilePicksterRank>
                        <MobileRankIcon>
                          <MobileRankText># {idx + 1}</MobileRankText>
                        </MobileRankIcon>
                        <MobilePicsterContent>
                          <ProfileImg mobile></ProfileImg>
                          <RankText mobile>{el?.name}</RankText>
                        </MobilePicsterContent>
                      </MobilePicksterRank>
                    </>
                  );
                })}
              </RankBox>
            </OverflowBox>
          </PicksterBoard>
        </MainTopBox>
        <MainMiddleBox>
          <PicksterText board>스포츠 카테고리</PicksterText>
          <MiddleFlex style={{ marginTop: 10 }}>
            {Category?.map((el, idx) => {
              return (
                <CategoryLink to={el?.link} style={{ marginRight: 10 }}>
                  <RankContent cate>
                    <Icon url={el?.img}></Icon>
                    <RankText>{el?.title}</RankText>
                  </RankContent>
                </CategoryLink>
              );
            })}
          </MiddleFlex>
        </MainMiddleBox>
        <MainBottomBox>
          <FitContent>
            <MainBottomTitle>
              <MainBottomText style={{ width: 70 }}></MainBottomText>
              <MainBottomText style={{ width: 120 }}>경기</MainBottomText>
              <MainBottomText style={{ width: 110 }}>시간</MainBottomText>
              <MainBottomText style={{ width: 160 }}>상태</MainBottomText>
              <MainBottomText style={{ width: 170 }}>홈팀</MainBottomText>
              <MainBottomText style={{ width: 130 }}>점수</MainBottomText>
              <MainBottomText style={{ width: 260 }}>원정팀</MainBottomText>
              <MainBottomText style={{ width: 170 }}>하프타임</MainBottomText>
              <MainBottomText style={{ width: 120 }}>Top</MainBottomText>
            </MainBottomTitle>
            {Game?.map((el, idx) => {
              return (
                <>
                  <MainBottomContent>
                    <MainBottomText style={{ width: 70 }}></MainBottomText>
                    <MainBottomText bottom style={{ width: 120 }}>
                      JPND2
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 110 }}>
                      8:00
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 160 }}>
                      <StateBtn state={el?.state}>{el?.state}</StateBtn>
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 170 }}>
                      레알 마드리드
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 130 }}>
                      3:2
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 260 }}>
                      바르셀로나
                    </MainBottomText>
                    <MainBottomText bottom style={{ width: 170 }}>
                      1 - 0
                    </MainBottomText>
                    <MainBottomText
                      bottom
                      style={{ width: 120 }}
                    ></MainBottomText>
                  </MainBottomContent>
                </>
              );
            })}
          </FitContent>
        </MainBottomBox>
      </MainContainer>
    </>
  );
};

export default Main;
