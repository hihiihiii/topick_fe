import React, { useState } from "react";
import {
  BoardBox,
  BoardText,
  BoardTextArea,
  BoardWrapper,
} from "../../Styles/Board/Board";
import {
  Input,
  MiddleFlex,
  PickOverScroll,
  PicksterCompl,
  PicksterComplBtn,
  PicksterFlexRow,
  PicksterFree,
  PicksterFreeFlex,
  PicksterInput,
  PicksterMiddleFlex,
  PicksterMiddleText,
  PicksterMobileFlex,
  PicksterOption,
  PicksterPointText,
  PicksterSelect,
  PicksterSelectFlex,
  PicksterStapFlex,
  PicksterText,
  PickTable,
  PickTd,
  PickTh,
  PickThead,
  PickTr,
  PointInput,
  PointInputBox,
  TestScroll,
} from "../../Styles/PicksterPick";
import { Linked } from "../../Styles/Side";

const PicksterWrite = () => {
  const [data, setData] = useState({
    event: "none",
    league: "none",
    scheduledMatch: "none",
  });

  return (
    <>
      <BoardWrapper>
        <BoardBox>
          <BoardText>픽스터 PICK 작성하기</BoardText>
          <PickOverScroll>
            <PicksterStapFlex>
              <PicksterText>제목</PicksterText>
              <PicksterInput placeholder="제목을 입력해주세요"></PicksterInput>
            </PicksterStapFlex>
            <PicksterStapFlex>
              <PicksterText>경기선택</PicksterText>
              <PicksterSelectFlex>
                <PicksterSelect
                  style={{
                    color: data?.event === "none" ? "#a7a7a7" : "#000",
                  }}
                  onChange={(e) => {
                    setData({ ...data, event: e.target.value });
                  }}
                >
                  <PicksterOption value={"none"}>
                    종목을 선택해주세요
                  </PicksterOption>
                  <PicksterOption value={1}>축구</PicksterOption>
                  <PicksterOption value={2}>배구</PicksterOption>
                  <PicksterOption value={3}>농구</PicksterOption>
                  <PicksterOption value={4}>야구</PicksterOption>
                </PicksterSelect>
                <PicksterSelect
                  style={{
                    color: data?.league === "none" ? "#a7a7a7" : "#000",
                  }}
                  onChange={(e) => {
                    setData({ ...data, league: e.target.value });
                  }}
                >
                  <PicksterOption value={"none"}>
                    리그를 선택해주세요
                  </PicksterOption>
                  <PicksterOption value={1}>축구</PicksterOption>
                  <PicksterOption value={2}>배구</PicksterOption>
                </PicksterSelect>
                <PicksterSelect
                  style={{
                    color: data?.scheduledMatch === "none" ? "#a7a7a7" : "#000",
                  }}
                  onChange={(e) => {
                    setData({ ...data, scheduledMatch: e.target.value });
                  }}
                >
                  <PicksterOption value={"none"}>
                    예정 경기를 선택해주세요
                  </PicksterOption>
                  <PicksterOption value={1}>축구</PicksterOption>
                  <PicksterOption value={2}>배구</PicksterOption>
                </PicksterSelect>
              </PicksterSelectFlex>
            </PicksterStapFlex>
            <MiddleFlex>
              <PicksterStapFlex>
                <PicksterMiddleFlex>
                  <PicksterFlexRow>
                    <PicksterMiddleText>투자포인트</PicksterMiddleText>
                    <PicksterPointText>
                      (현재 보유 포인트 : 10000p)
                    </PicksterPointText>
                  </PicksterFlexRow>
                </PicksterMiddleFlex>
                <PointInputBox>
                  <PointInput placeholder="포인트를 입력하세요"></PointInput>
                  <PicksterMiddleText>P</PicksterMiddleText>
                </PointInputBox>
              </PicksterStapFlex>

              <PicksterStapFlex>
                <PicksterMiddleFlex>
                  <PicksterFlexRow>
                    <PicksterMiddleText>구매가 선택</PicksterMiddleText>
                  </PicksterFlexRow>
                  <PicksterFreeFlex>
                    <PicksterFree>무료</PicksterFree>
                    <PicksterFree>유료</PicksterFree>
                  </PicksterFreeFlex>
                </PicksterMiddleFlex>
              </PicksterStapFlex>

              <PicksterStapFlex>
                <PicksterMiddleFlex>
                  <PicksterFlexRow>
                    <PicksterMiddleText></PicksterMiddleText>
                  </PicksterFlexRow>
                </PicksterMiddleFlex>
                <Input placeholder="구매가를 입력하세요."></Input>
              </PicksterStapFlex>
            </MiddleFlex>
            <PicksterStapFlex>
              <PicksterText>픽스터 PICK</PicksterText>
              <TestScroll>
                <PickTable>
                  <PickThead>
                    <PickTh th></PickTh>
                    <PickTh>팀1</PickTh>
                    <PickTh>무 / 기준</PickTh>
                    <PickTh>팀2</PickTh>
                  </PickThead>
                  <PickTr>
                    <PickTd td>승무패</PickTd>
                    <PickTd>승 1.577</PickTd>
                    <PickTd>팀 2</PickTd>
                    <PickTd>팀 4</PickTd>
                  </PickTr>
                  <PickTr>
                    <PickTd td>핸디캡</PickTd>
                    <PickTd>승 1.577</PickTd>
                    <PickTd>팀 2</PickTd>
                    <PickTd>팀 4</PickTd>
                  </PickTr>
                  <PickTr>
                    <PickTd td>더블찬스</PickTd>
                    <PickTd>승 1.577</PickTd>
                    <PickTd>팀 2</PickTd>
                    <PickTd>팀 4</PickTd>
                  </PickTr>
                  <PickTr>
                    <PickTd td>0 핸디</PickTd>
                    <PickTd>승 1.577</PickTd>
                    <PickTd>팀 2</PickTd>
                    <PickTd>팀 4</PickTd>
                  </PickTr>
                  <PickTr>
                    <PickTd td>언오버</PickTd>
                    <PickTd>승 1.577</PickTd>
                    <PickTd>팀 2</PickTd>
                    <PickTd>팀 4</PickTd>
                  </PickTr>
                </PickTable>
              </TestScroll>
            </PicksterStapFlex>
            <PicksterStapFlex>
              <PicksterText>내용</PicksterText>
              <BoardTextArea placeholder="내용을 입력하세요."></BoardTextArea>
            </PicksterStapFlex>
          </PickOverScroll>

          <PicksterCompl
            to="/pick"
            onClick={() => {
              window.scroll(0, 0);
            }}
          >
            <PicksterComplBtn>완료</PicksterComplBtn>
          </PicksterCompl>
        </BoardBox>
      </BoardWrapper>
    </>
  );
};

export default PicksterWrite;
