import React, { useState } from "react";
import { BackgroundLogo, Drop } from "../../Assets/file";
import {
  BackgoundImg,
  BackgroundFlex,
  DropIcon,
  HeaderIcon,
  HeaderTitleFlex,
  PickBox,
  PickContainer,
  PicksterBackground,
  PickSterBottom,
  PickSterButton,
  PickSterContent,
  PicksterDrop,
  PicksterDropMobile,
  PicksterInputBox,
  PicksterMobile,
  PicksterMobileColumn,
  PickSterProfileImg,
  PickSterText,
  PickSterTitle,
  PickSterTitleBox,
  PicksterWriteBox,
  PickWrapper,
} from "../../Styles/PicksterPick";
import { Linked } from "../../Styles/Side";

const Pick = () => {
  const [showMenu, setShowMenu] = useState(false);
  const [data, setData] = useState([
    { id: 1, check: false },
    { id: 2, check: false },
    { id: 3, check: false },
    { id: 4, check: false },
  ]);

  const buttonInfo = [
    {
      title: "팔로우",
      action: false,
    },
    {
      title: "블로그",
      action: false,
    },

    {
      title: "쪽지 보내기",
      action: false,
    },
    {
      title: "신고하기",
      action: false,
    },
  ];

  const set =
    "있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며, 품에 때까지 듣는다. 길지 두손을 못하다 것은 있는꽃이 듣기만 구할 청춘의 칼이다. 찾아 이상의 방황하였으며,석가는 운다. 내는 트고, 간에 목숨이 얼음 것이다. 하였으며,없으면, 그러므로 천하를 그들의 방황하였으며, 것이다.";

  //마우스 박스 아웃시 사라짐
  const _handleMouseOut = (idx) => {
    const datas = [...data];
    datas[idx].check = false;
    setData(datas);
  };

  //프로필 입력시 드롭다운 메뉴
  const _handlerMenu = (idx) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };
  return (
    <>
      <PicksterBackground>
        <PickWrapper>
          <PickContainer>
            <Linked to="/board/write">
              <PicksterWriteBox>
                {/* <PickSterProfileImg></PickSterProfileImg> */}
                <PicksterInputBox>게시글 작성하기</PicksterInputBox>
              </PicksterWriteBox>
            </Linked>
            {data?.map((el, idx) => {
              return (
                <>
                  <PickBox>
                    {/* 모바일 */}
                    <PicksterMobile
                      onClick={() => {
                        _handlerMenu(idx);
                      }}
                    >
                      <PickSterProfileImg></PickSterProfileImg>
                      <PicksterMobileColumn>
                        <PickSterText>hihihihi</PickSterText>
                        <PickSterText date>2021.09.10 </PickSterText>
                      </PicksterMobileColumn>
                    </PicksterMobile>
                    {el?.check && (
                      <PicksterDropMobile>
                        {buttonInfo?.map((el, idx) => {
                          return (
                            <PickSterButton
                              onClick={() => {
                                setShowMenu(false);
                              }}
                            >
                              {el?.title}
                            </PickSterButton>
                          );
                        })}
                      </PicksterDropMobile>
                    )}
                    {/* ////////////////////////////////////// */}
                    {/* <Linked to="/board/detail"> */}
                    <PickSterTitleBox>
                      <HeaderTitleFlex>
                        <PickSterTitle>레알 진짜 미친거 아니야?</PickSterTitle>
                        <PickSterText date>2021-11-03 10:10</PickSterText>
                      </HeaderTitleFlex>

                      <PickSterBottom>
                        <HeaderIcon content></HeaderIcon>
                        <PickSterText>hihihihi</PickSterText>

                        <DropIcon
                          src={Drop}
                          onClick={() => {
                            _handlerMenu(idx);
                          }}
                        ></DropIcon>
                        {el?.check && (
                          <PicksterDrop
                            onMouseLeave={() => {
                              _handleMouseOut(idx);
                            }}
                          >
                            {buttonInfo?.map((el, idx) => {
                              return (
                                <PickSterButton>{el?.title}</PickSterButton>
                              );
                            })}
                          </PicksterDrop>
                        )}
                      </PickSterBottom>
                    </PickSterTitleBox>
                    <PickSterContent>
                      &nbsp;
                      {set?.length > 100 ? set?.substring(0, 190) + "..." : set}
                    </PickSterContent>

                    {/* </Linked> */}
                    {/* <PickSterBottom
                      onClick={() => {
                        _handlerMenu(idx);
                      }}
                    >
                      <HeaderIcon content></HeaderIcon>
                      <PickSterText>hihihihi</PickSterText>
                    </PickSterBottom>

                    {el?.check && (
                      <PicksterDrop
                        onMouseLeave={() => {
                          _handleMouseOut(idx);
                        }}
                      >
                        {buttonInfo?.map((el, idx) => {
                          return (
                            <PickSterButton
                            // action={
                            //   el?.title === "팔로우" ||
                            //   el?.title === "신고하기"
                            //     ? true
                            //     : false
                            // }
                            >
                              {el?.title}
                            </PickSterButton>
                          );
                        })}
                      </PicksterDrop>
                    )} */}
                  </PickBox>
                </>
              );
            })}
          </PickContainer>

          {/* <BackgoundImg url={BackgroundLogo} /> */}
        </PickWrapper>
      </PicksterBackground>
    </>
  );
};

export default Pick;
