import styled from "@emotion/styled";
import React from "react";
import {
  BoardBox,
  BoardFlexEnd,
  BoardInputBox,
  BoardMarginTop,
  BoardText,
  BoardTextArea,
  BoardWrapper,
  CompletionBtn,
} from "../../Styles/Board/Board";
import { Linked } from "../../Styles/Side";

const ContactUs = () => {
  return (
    <>
      <BoardWrapper>
        <BoardBox>
          <BoardText>문의하기</BoardText>
          <BoardMarginTop>
            <BoardText strap>제목</BoardText>
            <BoardInputBox placeholder="제목을 입력해주세요"></BoardInputBox>
          </BoardMarginTop>
          <BoardMarginTop>
            <BoardText strap>내용</BoardText>
            <BoardTextArea placeholder="내용을 입력해주세요..."></BoardTextArea>
          </BoardMarginTop>
          <BoardFlexEnd>
            <Linked more to="/profile/contactdetails">
              <CompletionBtn>완료</CompletionBtn>
            </Linked>
          </BoardFlexEnd>
        </BoardBox>
      </BoardWrapper>
    </>
  );
};

export default ContactUs;
