import React, { useState } from "react";
import { Baseball, Basketball, Soccer, Volleyball } from "../../Assets/file";
import {
  BoardCategoryContainer,
  BoardContainer,
  BoardCountBox,
  BoardImgBox,
  BoardTitle,
  BoardWrapper,
  CategoryTitle,
  OverFlowX,
} from "../../Styles/PicksterBoard";
import {
  CategoryLink,
  Icon,
  MiddleFlex,
  RankContent,
  RankText,
} from "../../Styles/Global";
import {
  PickBox,
  PickSterBottom,
  PickSterButton,
  PickSterContent,
  PicksterDrop,
  PicksterDropMobile,
  PicksterInputBox,
  PicksterMobile,
  PicksterMobileColumn,
  PickSterProfileImg,
  PickSterText,
  PickSterTitle,
  PickSterTitleBox,
  PicksterWriteBox,
} from "../../Styles/PicksterPick";
import { Linked } from "../../Styles/Side";

const PicksterBoard = () => {
  const Category = [
    { title: "축구", img: Soccer, link: "/Soccer" },
    { title: "야구", img: Basketball, link: "/Basketball" },
    { title: "농구", img: Baseball, link: "/Baseball" },
    { title: "배구", img: Volleyball, link: "/Volleyball" },
  ];

  // const [showMenu, setShowMenu] = useState([{check : false}]);
  const [data, setData] = useState([
    { id: 1, check: false },
    { id: 2, check: false },
    { id: 3, check: false },
  ]);

  const buttonInfo = [
    {
      title: "팔로우",
      action: false,
    },
    {
      title: "블로그",
      action: false,
    },

    {
      title: "쪽지 보내기",
      action: false,
    },
    {
      title: "신고하기",
      action: false,
    },
  ];

  const set =
    "있을 꾸며 피가 바이며, 칼이다. 그들은 그러므로 얼음과 튼튼하며, 품에 때까지 듣는다. 길지 두손을 못하다 것은 있는꽃이 듣기만 구할 청춘의 칼이다. 찾아 이상의 방황하였으며,석가는 운다. 내는 트고, 간에 목숨이 얼음 것이다. 하였으며,없으면, 그러므로 천하를 그들의 방황하였으며, 것이다.";

  // const handler = handleDropMenu({ data, setData, idx });

  // 핸들 체크
  // const _handlerMenu = (idx) => {
  //   for (let i = 0; i < data.length; i++) {
  //     if (data[i].check) {
  //       data[i].check = false;
  //     }
  //   }
  //   const datas = [...data];
  //   datas[idx].check = !data[idx].check;
  //   setData(datas);
  // };

  const _handlerMenu = (idx) => {
    for (let i = 0; i < data.length; i++) {
      if (data[i].check) {
        data[i].check = false;
      }
    }
    const datas = [...data];
    datas[idx].check = !data[idx].check;
    setData(datas);
  };

  const _handleMouseOut = (idx) => {
    const datas = [...data];
    datas[idx].check = false;
    setData(datas);
  };

  return (
    <>
      <BoardWrapper>
        <BoardContainer>
          <BoardTitle>픽스터 PICK</BoardTitle>
          <BoardImgBox></BoardImgBox>
          <Linked to="/pick/write">
            <PicksterWriteBox>
              <PickSterProfileImg></PickSterProfileImg>
              <PicksterInputBox>게시물 작성하기</PicksterInputBox>
            </PicksterWriteBox>
          </Linked>

          <BoardCategoryContainer>
            <CategoryTitle>스포츠 카테고리</CategoryTitle>

            <OverFlowX>
              <MiddleFlex style={{ marginTop: 10 }}>
                {Category?.map((el, idx) => {
                  return (
                    <CategoryLink board style={{ marginRight: 10 }}>
                      <RankContent cate>
                        <Icon url={el?.img}></Icon>
                        <RankText>{el?.title}</RankText>
                      </RankContent>
                    </CategoryLink>
                  );
                })}
              </MiddleFlex>
            </OverFlowX>
          </BoardCategoryContainer>
          <BoardCountBox>총 게시물 {data?.length}개</BoardCountBox>

          {data?.map((el, idx) => {
            return (
              <>
                <PickBox style={{ marginTop: 20 }}>
                  <PicksterMobile
                    onClick={() => {
                      _handlerMenu(idx);
                    }}
                  >
                    <PickSterProfileImg></PickSterProfileImg>
                    <PicksterMobileColumn>
                      <PickSterText>dlduddls</PickSterText>
                      <PickSterText date>2021.09.10</PickSterText>
                    </PicksterMobileColumn>
                  </PicksterMobile>
                  {el?.check && (
                    <PicksterDropMobile
                      onClick={() => {
                        _handleMouseOut(idx);
                      }}
                    >
                      {buttonInfo?.map((el, idx) => {
                        return <PickSterButton>{el?.title}</PickSterButton>;
                      })}
                    </PicksterDropMobile>
                  )}

                  <Linked to="/pick/detail">
                    <PickSterTitleBox>
                      <PickSterTitle>레알 진짜 미친거 아니야?</PickSterTitle>
                      <PickSterText date>2021.09.10</PickSterText>
                    </PickSterTitleBox>
                    <PickSterContent>
                      {set?.length > 100 ? set?.substring(0, 100) + "..." : set}
                    </PickSterContent>
                  </Linked>

                  <PickSterBottom
                    onClick={() => {
                      _handlerMenu(idx);
                    }}
                  >
                    <PickSterProfileImg></PickSterProfileImg>
                    <PickSterText>dlduddls</PickSterText>
                  </PickSterBottom>
                  {el?.check && (
                    <PicksterDrop
                      onMouseLeave={() => {
                        _handleMouseOut(idx);
                      }}
                    >
                      {buttonInfo?.map((el, idx) => {
                        return <PickSterButton>{el?.title}</PickSterButton>;
                      })}
                    </PicksterDrop>
                  )}
                </PickBox>
              </>
            );
          })}
        </BoardContainer>
      </BoardWrapper>
    </>
  );
};

export default PicksterBoard;
