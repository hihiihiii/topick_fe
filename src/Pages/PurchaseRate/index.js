import React, { useState } from "react";
import {
  PurchaseContainer,
  PurchaseTitle,
  PurchaseTop_tbody,
  PurchaseTop_td,
  PurchaseTop_th,
  PurchaseTop_thead,
  PurchaseWrapper,
  Purchase_Table,
  ResultBox,
  ResultText,
} from "../../Styles/PurchaseRate";

import { OverFlowX } from "../../Styles/PicksterBoard";

const PurchaseRate = () => {
  const [data, setData] = useState([{}, {}]);
  return (
    <>
      <PurchaseWrapper>
        <PurchaseContainer>
          <PurchaseTitle title>국내 구매율</PurchaseTitle>
          <OverFlowX>
            <Purchase_Table>
              <PurchaseTop_thead>
                <PurchaseTop_th style={{ width: 125 }}>경기</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 175 }}>시간</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 300 }}>
                  홈팀 vs 원정팀
                </PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>승</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 100 }}>무</PurchaseTop_th>
                <PurchaseTop_th style={{ width: 85 }}>패</PurchaseTop_th>
              </PurchaseTop_thead>
              {data?.map((el, idx) => {
                return (
                  <PurchaseTop_tbody>
                    <PurchaseTop_td style={{ width: 125 }}>
                      JPND2
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 175 }}>
                      2021.09.01
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 265 }}>
                      레알마드리드 vs 레알 마드리드
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <ResultBox>
                        <ResultText>55%</ResultText>
                        <ResultText>220,000</ResultText>
                      </ResultBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <ResultBox>
                        <ResultText>55%</ResultText>
                        <ResultText>220,000</ResultText>
                      </ResultBox>
                    </PurchaseTop_td>
                    <PurchaseTop_td style={{ width: 100 }}>
                      <ResultBox>
                        <ResultText>55%</ResultText>
                        <ResultText>220,000</ResultText>
                      </ResultBox>
                    </PurchaseTop_td>
                  </PurchaseTop_tbody>
                );
              })}
            </Purchase_Table>
          </OverFlowX>
        </PurchaseContainer>
      </PurchaseWrapper>
    </>
  );
};

export default PurchaseRate;
