import React from "react";
import {
  Container,
  MainBox,
  MainInputBox,
  MainText,
  Test,
  Logo,
  Wrapper,
  BottomText,
  LogoImg,
  MainLogo,
} from "../../Styles/Global";
import { Logo_Main } from "../../Assets/file";
import { Link } from "react-router-dom";

const Login = () => {
  return (
    <>
      <Wrapper>
        {/* <Logo url={Logo_Main} /> */}
        <MainLogo>
          <LogoImg main src={Logo_Main}></LogoImg>
        </MainLogo>

        <Test>
          <Container>
            <MainText title center>
              로그인
            </MainText>
            <MainText style={{ marginTop: 50 }}>이메일</MainText>
            <MainInputBox placeholder="이메일을 입력하세요"></MainInputBox>
            <MainText style={{ marginTop: 25 }}>비밀번호</MainText>
            <MainInputBox
              type="password"
              placeholder="비밀번호를 입력하세요"
            ></MainInputBox>
            <Link to="/" style={{ textDecoration: "none" }}>
              <MainBox style={{ marginTop: 55 }}>로그인</MainBox>
            </Link>
            <Link to="/register" style={{ textDecoration: "none" }}>
              <MainBox color back style={{ marginTop: 20 }}>
                회원가입
              </MainBox>
            </Link>
          </Container>
        </Test>
        <BottomText>© 2021 TOPICK</BottomText>
      </Wrapper>
    </>
  );
};

const Register = () => {
  return (
    <Wrapper>
      {/* <Logo url={Logo_Main} /> */}
      <MainLogo>
        <LogoImg main src={Logo_Main}></LogoImg>
      </MainLogo>
      <Test>
        <Container>
          <MainText title center>
            회원가입
          </MainText>
          <MainText style={{ marginTop: 50 }}>이메일</MainText>
          <MainInputBox placeholder="이메일을 입력하세요"></MainInputBox>
          <MainText style={{ marginTop: 25 }}>닉네임</MainText>
          <MainInputBox placeholder="닉네임을 입력하세요"></MainInputBox>
          <MainText style={{ marginTop: 25 }}>비밀번호</MainText>
          <MainInputBox
            type="password"
            placeholder="비밀번호를 입력하세요"
          ></MainInputBox>
          <MainText style={{ marginTop: 25 }}>비밀번호 확인</MainText>
          <MainInputBox
            type="password"
            placeholder="비밀번호를 확인해주세요"
          ></MainInputBox>
          <Link to="/login" style={{ textDecoration: "none" }}>
            <MainBox style={{ marginTop: 55 }}>회원가입</MainBox>
          </Link>
        </Container>
      </Test>
      <BottomText>© 2021 TOPICK</BottomText>
    </Wrapper>
  );
};

export { Login, Register };
