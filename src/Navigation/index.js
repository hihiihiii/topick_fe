import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { Login, Register } from "../Pages/Auth";
import Header from "../Components/Header";
import Chat from "../Components/Chat";
import Main from "../Pages/Main";
import Pick from "../Pages/Pick";
import TodayFact from "../Pages/TodayFact";
import PicksterBoard from "../Pages/PicksterBoard";
import PurchaseRate from "../Pages/PurchaseRate";
import Covers from "../Pages/Covers";
import Rank from "../Pages/Rank";
import Proto from "../Pages/Proto";
import LineUp from "../Pages/LineUp";
import DividendDropRate from "../Pages/DividendDropRate";
import BoardWrite from "../Pages/PicksterBoard/Write";
import BoardDetail from "../Pages/PicksterBoard/Detail";
import BoardPaidDetail from "../Pages/PicksterBoard/PaidDetail";
import PicksterWrite from "../Pages/Pick/PicksterWrite";

import Profile from "../Components/Profile";
import ContactUs from "../Pages/Contact/ContactUs";
import ContactDetails from "../Components/MyPage/ContactDetails";
import ContactDetail from "../Pages/Contact/ContactDetail";
import PointCharge from "../Pages/PointCharge";
import MyProfile from "../Components/MyPage/MyProfile";
import MyBlog from "../Components/MyPage/MyBlog";
import PayPickster from "../Components/MyPage/PayPickster";
import Point from "../Components/MyPage/Point";
import SalesHistory from "../Components/MyPage/SalesHistory";
import PurchaseHistroy from "../Components/MyPage/PurchaseHistroy";
import PicksterDetails from "../Pages/Pick/PicksterDetails";
import PicksterDetail from "../Pages/Pick/PicksterDetails";

const Navigation = () => {
  return (
    <>
      <Router>
        <Header></Header>
        <Profile></Profile>
        <Switch>
          <Route exact path="/" component={Main} />
          {/* 로그인 */}
          <Route exact path="/login" component={Login} />
          {/* 회원가입 */}
          <Route exact path="/register" component={Register} />
          {/* 마이페이지 */}
          {/* 1. 내 프로필 */}
          <Route exact path="/profile/myprofile" component={MyProfile} />
          {/* 2. 내 블로그 */}
          <Route exact path="/profile/myblog" component={MyBlog} />
          {/* 3. 내 유료 픽스터 */}
          <Route exact path="/profile/paypickster" component={PayPickster} />
          {/* 4. 포인트 */}
          <Route exact path="/profile/point" component={Point} />
          {/* 5. 판매 내역 */}
          <Route exact path="/profile/saleshistory" component={SalesHistory} />
          {/* 6. 구매 내역 */}
          <Route
            exact
            path="/profile/purchasehistroy"
            component={PurchaseHistroy}
          />
          {/* 7. 문의 내역 */}
          <Route
            exact
            path="/profile/contactdetails"
            component={ContactDetails}
          />
          {/* 공지사항 */}
          <Route exact path="/board" component={Pick} />
          {/* 글작성 */}
          <Route exact path="/board/write" component={BoardWrite} />
          {/* 게시판 상세보기 */} {/* (픽스터 상세보기) */}
          <Route exact path="/board/detail" component={BoardDetail} />
          {/* 오늘의 팩트체크 */}
          <Route exact path="/todayfact" component={TodayFact} />
          {/* 픽스터 Pick 게시판 */}
          <Route exact path="/pick" component={PicksterBoard} />
          {/* 픽스터 Pick 게시판 작성 */}
          <Route exact path="/pick/write" component={PicksterWrite} />
          {/* 픽스터 Pick 게시판 상세보기 */} {/* (게시판 상세보기) */}
          <Route exact path="/pick/detail" component={PicksterDetail} />
          {/* 픽스터픽 유료 상세보기 */}
          <Route path="/pick/paiddetail" component={BoardPaidDetail} />
          {/* 문의하기 */}
          <Route path="/contact/write" component={ContactUs} />
          {/* 문의내용 상세 보기 */}
          <Route path="/contact/detail" component={ContactDetail} />
          {/* 포인트 충전 */}
          <Route path="/pointcharge" component={PointCharge} />
          {/* 국내구매율 */}
          <Route path="/purchaserate" component={PurchaseRate} />
          {/* 커버스 픽 */}
          <Route path="/covers" component={Covers} />
          {/* 픽스터 랭킹 */}
          <Route path="/rank" component={Rank} />
          {/* 프로토 */}
          <Route path="/proto" component={Proto} />
          {/* 라인업 */}
          <Route path="/lineup" component={LineUp} />
          {/* 배당 하락율 */}
          <Route path="/dividenddroprate" component={DividendDropRate} />
        </Switch>
      </Router>
    </>
  );
};

export default Navigation;
